\section{Extended PHEMAP}
\label{sec:ephemap}
Although the original PHEMAP design is a consolidated solution for mutual authentication for highly constrained devices, it is not suited for a typical CE IoT scenario, where a multitude of terminal devices generate and process data that must be communicated at several levels in a fast and secure way. In this scenario in fact, the centralized verifier would easily become a bottleneck and would negatively impact on the overall latency, due to the high number of authentication requests typically issued. 
Another limitation of the original PHEMAP design is that it only enables mutual authentication between a device embedding a PUF and a verifier, while neglecting the direct communications between two peer terminal devices, both equipped with a PUF. 

In order to overcome the above limitations, this work aims to propose an extended version of PHEMAP that enables to establish mutual authentication among the edge devices of a CE-based IoT system, namely terminal nodes and gateways, and delegate part of the authentication service to this nodes in order to relieve the burden of computation for the verifier.

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figures/roles.pdf}
	\caption{new roles introduced by Extended PHEMAP}
	\label{fig:ephemap_roles}
\end{figure}

The mutual authentication scheme, in particular, includes the following two protocols:

\begin{itemize}
	\item \textit{Salted PHEMAP}: enables to achieve mutual authentication between a gateway and a terminal device;
	\item \textit{Babel-Chain PHEMAP}: enables to achieve mutual authentication between two terminal devices.
\end{itemize}


%\subsubsection{General assumptions}
\noindent In addition to the assumptions reported in Section \ref{sec:ph} regarding the features of the adopted PUF architectures (i.e., adopted PUFs are strong, tamper-resistant and reliable), the proposed scheme assumes that:

\begin{itemize}
	\item Terminal nodes are equipped with a PUF circuit and have limited computation and storage resources. 
	\item Gateways have moderate computation and storage capabilities, such that some cryptographic primitive can be implemented.
	\item Terminal nodes are logically organized in subgroups, and each subgroup is connected to a reference gateway, which is responsible for their management. 
	\item Terminal nodes can only communicate with the authentication service, with their reference gateway and with the other terminal nodes belonging to the same subgroup. 
	%all the communications among the terminal nodes belonging to the same subgroup and all the communications among each gateway and the underlying terminal nodes must be authenticated. 
	Communication among terminal nodes belonging to different subgroups is out of the scope of this work.
	\item The enrollment of terminal devices' PUFs is carried out in a secure environment and the extracted chains are securely transferred to an
	an authentication service, hosted on the cloud, which stores them in a secure database.
	\item The authentication service is responsible for the initialization of all the terminal nodes (according to the PHEMAP protocol discussed above) before operation, and of their re-initialization, whenever needed, during operation.
	%\item all the communications among the authentication service and the terminal nodes/gateways must be authenticated;
	\item The communication channel connecting different gateways and the gateways with the authentication service is secure.
\end{itemize}

%\subsubsection{Attacker model}
\noindent The considered attacker model can be summarized as follows:
\begin{itemize}
	\item An attacker cannot access the PUF circuitry embedded in a terminal device.
	\item An attacker can eavesdrop on any communication channel.
	\item An attacker can physically capture a gateway or terminal node and make them act as malicious entities.
	%\item When capturing a terminal device, an attacker cannot alter the content of the \textit{Q} and \textit{SR} hardware registers but may potentially read the content of \textit{Q}.
	\item An attacker can try to impersonate both terminal and gateway nodes and the authentication service by either forging messages or replaying previously intercepted messages.
	\item Colluding attackers are not considered.
\end{itemize}
%is a pair-wise protocol that involves a device, which embeds a strong PUF circuitry, and a verifier, which enrolls the device PUF collecting chains of links to use at mission time, and guarantees between them a mutual authentication procedure.
%However, in a distributed scenario, PHEMAP turns out to be limited since the verifier is not able to delegate authentications to third-parties.
%\msg{Mario}{Ale}{Ci vorrebbe un esempio di dominio, come traffic lights}


%scrievere che fra AS e gateway c'è canale sicuro
%Both protocols entail a partial delegation of the authentication capabilities of the authentication server to the edge devices.

%is a pair-wise protocol that involves a device, which embeds a strong PUF circuitry, and a verifier, which enrolls the device PUF collecting chains of links to use at mission time, and guarantees between them a mutual authentication procedure.
%However, in a distributed scenario, PHEMAP turns out to be limited since the verifier is not able to delegate authentications to third-parties.
%\msg{Mario}{Ale}{Ci vorrebbe un esempio di dominio, come traffic lights}

%Both protocols entail a partial delegation of the authentication capabilities of the authentication server to the edge devices.

\subsection{Salted PHEMAP}
\label{subsec:salted}

\textit{Salted PHEMAP} enables a terminal node and its respective gateway to mutually prove their identities while communicating. 
The protocol is based on a partial delegation of the authentication capabilities of the authentication server to the gateway nodes. 
Basically, with \textit{Salted PHEMAP}, the authentication service transfers part of the enrolled chains to gateways, which act as \textit{local verifiers} for the underlying terminal nodes.\\

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{salted.pdf}
	\caption{Salted PHEMAP scenario}
	\label{fig:salted_scenario}
\end{figure}

Let D be a terminal device, G be the gateway connected to D and AS be the authentication service. 
Let $\gamma_{D, l_0, M}$ be one of the chains enrolled for device D stored by AS. The chain portion obtained by extracting $T<M$ consecutive links from the chain $\gamma_{D,l_0,M}$ starting from a link $t_0$ is called \textit{carnet} and is denoted with $\tau_{D,t_0,T}$. 
%is identified with notation $\tau_{D,l_0,M,t_0,T}$.
$T$ is the \textit{carnet length} and the links $\left[t_0, \cdots, t_{T-1}\right]$ included in the carnet are called \textit{tickets}.

Let us assume that D and AS are synchronized on link $l_{i-1}$ of chain $\gamma_{D,l_0,M}$ after the PHEMAP basic initialization procedure (i.e., D stores value $l_{i-1}$ in its secure register \textit{Q}), and let us assume that node D wants to communicate with G for the first time (or vice-versa). 
Salted PHEMAP requires a setup procedure involving the exchange of three authenticated messages between D and AS and one authenticated message between AS and G. Authentication between terminal nodes and the authentication service is achieved by relying upon the basic PHEMAP verification scheme, namely by exchanging a number of consecutive non-sentinel chain links. Overall, it involves four subsequent links (three are exchanged in clear-text following basic verification and one is used to protect a salt value, as discussed in the following).  
%In particular, being $l_{i-1}$ the last consumed link, the setup procedure will involve the exchange of links $l_i$, $l_{i+1}$ and $l_{i+2}$. 
The \textit{Salted PHEMAP} setup entails the following steps:

\begin{figure}
	\centering
	\includegraphics[width=0.99\textwidth]{saltedphemap.pdf}
	\caption{Salted PHEMAP protocol interactions}
	\label{fig:salted_phemap}
\end{figure}

\begin{enumerate}
	\item D sends to AS a request to communicate with G. The request message $m_0$  includes value $\theta_D\left(Q\right) = \theta_D\left(l_{i-1}\right)$ (assuming it is not a sentinel), used to perform authentication according to PHEMAP principles. Such value is also stored in \textit{Q} by the device, updating previous content.
	
	\item AS verifies that the value included in the request corresponds to the non-sentinel link immediately following $l_{i-1}$ in the current chain (let us assume it is $l_i$). If the two values match, AS authenticates D and continues the setup procedure. Hence, it:
	
	\begin{enumerate}
		\item extracts a carnet $\tau_{D,t_0,T} = \left[t_0, \cdots, t_{T-1}\right]$ from chain $\gamma_{D, l_0, M}$ starting from ticket $t_0 = l_{i+4}$;
	%, and (ii) removes all sentinel links from it by obtaining the carnet $\tau_{D,t_0,T}$, with $T<L$. 
	 \item generates a random number $S_D$ for device D, denoted as the \textit{salt};
	 \item sends message $m_1 = \left(v_1,v_2\right) = \left(l_{i+1}, S_D \oplus l_{i+2}\right)$ to D, i.e., it sends a masked salt obtained by XOR-ing the salt with a further link of the chain.
	\end{enumerate}
	
	
	\item D first extracts $l_{i+1}$ from $m_1$ and checks whether it corresponds to $\theta_D\left(Q\right)$. If the two values match, the device authenticates AS and updates \textit{Q} with value $\theta_D\left(Q\right)$ (corresponding to $l_{i+1}$). 
	Then, the device:
	
	
	\begin{enumerate}
		\item computes $\theta_D\left(Q\right) \oplus v_2$ in order to obtain the salt $S_D$ and stores the salt in a secure register \textit{SR};
		%\item updates register \textit{Q} with the new value $\theta_D\left(Q\right)$ (corresponding to $l_{i+2}$);
	  \item computes $\theta_D^2\left(Q\right)$ (corresponding to $l_{i+3}$), stores this value in \textit{Q} 
	  and sends it in message $m_2$ to AS. It is worth noting that $l_{i+2}$ must not be stored in \textit{Q}, since a malicious observer might read the register and disclose the secret salt.
	  \item Finally, the device computes $\theta_D\left(Q\right) \oplus S_D$ (corresponding to $l_{i+4} \oplus S_D$) and stores this value in \textit{Q}. This step is needed since, from this moment on, the device will operate on salted values.

	\end{enumerate}
		
	\item AS checks whether $m_2$ corresponds to link $l_{i+3}$, which is the link immediately following the last consumed one. If the two values match, AS authenticates the device. Afterward, AS computes a \textit{salted carnet} $\chi_{D,x_0,T} = \left[x_0, \cdots, x_{T-1}\right]$ by XOR-ing the tickets $\left[t_0, \cdots, t_{T-1}\right]$ of the carnet $\tau_{D,t_0,T}$ obtained at step 1 with the salt $S_D$, and sends message $m_3 = \left(D, \chi_{D,x_0,T}\right)$ to node G.
\end{enumerate}

Steps 2 and 3 represent the so-called \textit{salt installation phase}, whose goal is to provide the device with a salt shared with the gateway. Note that register \textit{SR}, used to store the salt, cannot be read nor altered by external entities.
%Removing sentinels from transferred carnets is also useful in case of a malicious gateway: in absenc
%a che serve togliere sentinelle?
The \textit{salted carnet} $\chi_{D,x_0,T}$ received by node G is used for subsequent authentication operations between D and G. Note that, after setup, both the gateway and the device are synchronized on the first ticket of the salted carnet, namely $x_0$. 
The same interactions discussed in Section \ref{sec:ph} for the basic PHEMAP verification phase are carried out, with the main variant that the gateway does not use plain links to challenge (or respond to) the device, but salted tickets, i.e., links XOR-ed with a salt. The interactions described above are reported in Figure \ref{fig:salted_phemap}, where $d_0$ represents link $l_{i-1}$.

\begin{figure}
	\centering
	\includegraphics[width=0.6\textwidth]{salted_device.pdf}
	\caption{Schematic of device internal circuitry that supports salted PHEMAP protocol.}
	\label{fig:salted_device}
\end{figure}

The device architecture needed to run \textit{Salted PHEMAP} is depicted in Figure \ref{fig:salted_device}. As shown, a XOR gate is inserted before and after the PUF circuit, respectively. The XOR inserted before the PUF takes in input the challenge (corresponding to register \textit{Q}) and the output of a multiplexer, which provides either the constant zero or the salt, stored in the SR register. The select line of the multiplexer is connected to a signal (denoted as Salted/Non Salted in the figure) which is set by the control logic of the device: in particular, when the device communicates with the AS, like in the setup procedure, the signal is set to 0, so that the XOR gate has no effect, while it is set to 1 during communications with a gateway (i.e., communications carried out after setup). 

Let us remark that \textit{Q} and \textit{SR} reside in a secure tamper-resistant perimeter. In particular, as clarified later in the discussion about the security of the protocol, the access to \textit{SR} must be inhibited to any external entity (i.e., SR cannot be read from any external entity), while \textit{Q} does not hold confidential information and may be read without causing harm.
Beside \textit{SR} and \textit{Q}, there is also an additional register in the secure perimeter, denoted as \textit{SCount} in the figure. \textit{SCount} represents a counter used by the device to check the validity of the salt: when the salt is installed on the device, the counter is set to the value $T$, i.e., the carnet length. Each time the PUF is queried, the counter is decreased by one unit. When the counter goes to zero, the device simply invalidates the salt, by setting it to zero. The role of the counter is explained in the following subsection.


%to ensure the correct behavior: the ticket received from the gateway must first be XOR-ed with the salt to obtain the corresponding \textit{un-salted} chain link to be submitted to the PUF circuit. Then, the PUF response must be XOR-ed with the salt before being sent to the gateway, in order to in order to obtain a value that can be compared with the tickets available at the gateway.

%Contrary to BC-PHEMAP previously discusses (Section~\ref{subsec:bc}), it is not required to equip the TTD with a PUF circuit, even though it is required for devices to embed a register, called salt-register (SR), that resides in the secure perimeter, as depicted in Figure~\ref{fig:salted_device}.
%For instance, considering a verifier and a device that authenticate each other by means of PHEMAP and a TTD that cannot take part in PHEMAP as it lacks of PUF.
%The verifier could transfer to TTD, through a secure channel, a portion of a chain such that the PHEMAP protocol can be easily managed directly by the TTD.

%This scenario opens some issues related to the fact that getting a portion of chain that includes a sentinels allows to personify the verifier without having control on the TTD.
%%non posso mandare solo link senza sentinelle perchè poi ttp non potrebbe fare autenticazione perchè srvono le sentinelle
%For instance, let us considering the case in which the verifier send to TTD $\gamma_{D, c_0, M}$ with $M>2\times S$, where S is the sentinel period. TTD could reproduce the initialization procedure reported in Figure~\ref{fig:phemap_example} as times as it want without fulfilling the non-reuse of links rule and verifier would have no control over that.
%
%Salted PHEMAP faces with the control issue by introducing into the device an additional register which contains a salt value, called salt-register. Whenever the verifier want to delegate TTD for authenticating the device A, it calculates a random salt $k$ that it installs onto the SR of the device A.
%verifier manda salt a gateway su canale sicuro (ssh)
%verifier manda lo stesso salt con messaggio particolare in cui sta in XOR con un link non precedentemente scambiato



\subsubsection{Security considerations}

Based on the attacker model introduced before, an attacker may attempt to exploit exposed information and/or to capture physical nodes or compromise the authentication service in order to impersonate any of the involved parties and interfere with the normal operation of the system. 
%
As widely discussed in \cite{barbareschi2018puf}, the mechanism used by PHEMAP to initialize communication and perform verification, based on using chain links only once and on preventing the exchange of sentinel links, makes the PHEMAP protocol robust against man-in-the-middle attacks.
%
However, the extensions introduced with \textit{Salted PHEMAP} increase the opportunity for an attacker to cause harm, since more nodes are involved in communication and authentication capabilities are delegated to gateways.

Let us analyze the exposure of the information exchanged over communication channels and stored on the devices and the opportunity for an attacker to use it in order to carry out a successful attack. 
As discussed, AS and D simply exchange subsequent links of a chain to establish mutual authentication during salt installation, and this does not raise any security issue since exchanged links will never be re-used. During these exchanges, the salt is sent to the device XOR-ed with an undisclosed chain link, therefore it is impossible for an external attacker to steal it. Moreover, as mentioned before, the salt is stored in a secure register and cannot be altered nor read from attackers able to physically capture the device. 

At the end of the setup procedure, AS sends a salted carnet to G through a secure channel. As discussed before, the device's control logic forces the use of the salt during all the operations that involve the communication with its reference gateway. This ensures that the gateway can never impersonate the AS, as it would need plain links.

However, it is worth noting that, as the carnet consists of a number of $T$ subsequent salted links (including sentinels), G is granted the possibility not only to perform verification but also to trigger the initialization of the device, limited clearly to the number of available tickets.
While this delegation is fundamental to keep latency limited in case of network errors and packet losses, thanks to the ability of the gateway to re-initialize the device and save communication without requiring the intervention of AS, it may imply some security issues. 
Indeed, if the gateway is someway compromised, due to the limited capability of the device to keep status information (only the last consumed link is stored locally), G may trigger as many initializations as it wants and may be able to re-utilize old links multiple times, thus guaranteeing a continuous communication with the device. The counter SCount introduced previously is used exactly to prevent this possibility: when the counter goes to zero, the salt is simply invalidated (i.e., set to zero), so G would no longer be able to communicate with D.
 
%is necessary when the communication channel between AS and G is not confidential to protect a set of valid chain links of device D from being disclosed and used by a malicious entity to communicate with it. 
%Nevertheless, it must be noted that, if the carnet is intercepted by a malicious entity, this would be able to perform a number of successful authentications with node D, while however

Note that this mechanism may be used also in other cases, to exclude specific entities from the system if needed. In fact, suitable mechanisms may be implemented to periodically verify the legitimacy of an entity and/or to enable devices notify alerts to AS in case an unexpected behavior is detected (e.g., multiple authentication attempts between D and G fail). In this case, AS may simply trigger an initialization of the device, which would result in the invalidation of the installed salt.

\subsection{Babel-Chain PHEMAP}
\label{subsec:bc}
\textit{Babel-Chain (BC) PHEMAP} allows two heterogeneous devices equipped with a PUF to mutually authenticate with each another without an active participation of the authentication service.

In the basic PHEMAP, two devices that want to communicate must rely upon the verifier to convey authentication and data messages, since they do not have other means to prove their authenticity.
\textit{BC PHEMAP}, instead, leverages the concept of \textit{carnet of tickets}, introduced before, in order to let devices accomplish a limited number of authentication operations almost independently.

Let A and B be two terminal devices and let AS be the authentication service. Let $\gamma_{A, a_0, M}$ be one of the chains enrolled for device A and $\gamma_{B, b_0, M}$ be one of the chains enrolled for device B.  
Let $a_{i-1} \in \gamma_{A,a_0,M}$ be the link on which A and AS are synchronized and $b_{i-1} \in \gamma_{B,b_0,M}$ be the link on which B and AS are synchronized.

Let $\tau_{A,a_i,T} = \left[a_i, \cdots, a_{i+T-1}\right]$ be the carnet extracted from chain $\gamma_{A,a_0,M}$ starting from link $a_i$ and including $T$ links without sentinels, and let $\tau_{B,b_i,T} = \left[b_i, \cdots, b_{i+T-1}\right]$ be the carnet extracted from chain $\gamma_{B,b_0,M}$ starting from link $b_i$ and including $T$ links without sentinels.
 
A \textit{babel-chain carnet} (BC carnet) for the pair (A,B), denoted as $\tau^{BC}_{A,a_i,B,b_i,T}$, is obtained by combining the two above carnets as follows:

\begin{equation}
\begin{aligned}
\tau^{BC}_{A,a_i,B,b_i,T} = \left\lbrace x^{BC}_h = a_h \oplus b_h |_{h=i \dots (i+T-1)}\right\rbrace.
\end{aligned}
\end{equation}\\

%For instance, a BC carnet of 4 tickets for device A and B is: $BC_{A,a_0,B,b_0,4} = \left\lbrace a_1 \oplus b_1;\; a_2 \oplus b_2;\; a_3 \oplus b_3;\; a_4 \oplus b_4 \right\rbrace$.

\noindent Let us assume that A wants to communicate with B for the first time. \textit{BC PHEMAP} setup requires the exchange of two authenticated messages between A and AS, which involves the use of links $a_i$ and $a_{i+1}$. For the sake of simplicity, let us describe the involved steps without explicitly reporting the links exchanged for authentication purposes. The setup requires then the following operations:

\begin{enumerate}
	\item A sends a request to communicate with B to AS, in which it can also specify a number of expected interactions $T$ (i.e., the number of links needed to send the expected number of messages). If this information is not provided, a default $T$ is selected;
	
	\item AS extracts the carnet $\tau_{A,a_i,T} = \left[a_i, \cdots, a_{i+T-1}\right]$ from chain $\gamma_{A,a_0,M}$ and the carnet $\tau_{B,b_i,T} = \left[b_i, \cdots, b_{i+T-1}\right]$ from chain $\gamma_{B,b_0,M}$ by removing all sentinel links. Afterward, it generates the BC carnet $\tau^{BC}_{A,a_i,B,b_i,T}$ as specified above and sends it to A;
	
	\item A stores the received BC carnet in its local memory.	
	
\end{enumerate}



\noindent Note that, as remarked later in the security discussion section, $\tau^{BC}_{A,a_i,B,b_i,T}$ does not disclose sensitive information about chains of devices A and B, therefore BC carnets can be exchanged in clear-text and can be stored in local memories without jeopardizing the protocol security.

\noindent Subsequent communications are carried out in the following way:
\begin{enumerate}
	\item A sends message $m_1 = \left(x_i \oplus a_i\right) = \left(\left(a_i \oplus b_i\right) \oplus a_i\right)$ to B;
	\item B checks whether $m_1$ corresponds to $\theta_B\left(b_{i-1}\right)=b_i$. If the two values match, B authenticates A. Afterward, B computes $\theta_B\left(b_i\right)$, stores this value in its register $Q$, and sends $m_2 = \theta_B\left(b_i\right)$ to A;
	\item A checks whether $m_2$ corresponds to $x_{i+1} \oplus a_{i+1} = \left(\left(a_{i+1} \oplus b_{i+1}\right) \oplus a_{i+1}\right)$. If the two values match, A authenticates B.
\end{enumerate}

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{figures/bcphemap_naware.pdf}
	\caption{BC PHEMAP protocol interactions}
	\label{fig:bc-naware}
\end{figure}



Note that the described protocol is particularly suited to those (real-world) situations in which terminal devices are heterogeneous in terms of available resources, and some more powerful devices (e.g., simple micro-controllers) periodically query several simpler devices (e.g., sensors) to obtain data. In this case, the simpler devices can keep very limited the consumption of local resources by implementing, de facto, the basic PHEMAP protocol (it is the case of device B), while the more powerful ones can sacrifice some storage and computation capabilities to manage BC carnets. In particular, in addition to the basic PUF architecture used in PHEMAP, the second type of device must include a XOR gate to perform operations on the tickets and must embed a small buffer for storing the carnet.

In other scenarios, where the two interacting terminal devices are both too constrained to manage carnets, their reference gateway may be used to convey messages and enable mutual authentication. 
This case is managed by a variant of the \textit{BC PHEMAP} protocol named \textit{Proxy-based BC PHEMAP}.\\

\begin{figure}[H]
	\centering
	\includegraphics[width=0.99\textwidth]{figures/babelchain_eng.pdf}
	\caption{BC PHEMAP variants}
	\label{fig:babelchain_scenarios}
\end{figure}

Let us assume that device A wants to start a communication with B for the first time. During \textit{Proxy-based BC PHEMAP} setup, A sends a request to communicate with G to AS. The authentication service generates a BC carnet as in \textit{BC PHEMAP} and sends it to G. Assuming that A and B are synchronized on links $a_{i-1}$ and $b_{i-1}$ respectively with the verifier, the subsequent verification phase entails the following steps:

\begin{enumerate}
	\item A computes $m_1 = \theta_A\left(a_{i-1}\right)$ (corresponding to $a_i$), stores this value in its register \textit{Q} and sends it to G;
	\item G picks ticket $x^{BC}_i = a_i \oplus b_i$ from the BC carnet and sends $m_2 = \left(x^{BC}_i \oplus m_1\right)$ to B;
	\item B checks whether $m_2$ corresponds to the value $\theta_B\left(b_{i-1}\right)$ (representing $b_i$). If the two values match, it computes $m_4 = \theta_B\left(b_i\right)$, stores this value in its register $Q$ and sends it to G. 
	\item G picks ticket $x^{BC}_{i+1} = a_{i+1} \oplus b_{i+1}$ from the BC carnet and sends $m_5 =  \left(x^{BC}_{i+1} \oplus m_4 \right)$ to A;
	\item A checks whether $m_5$ corresponds to the value $\theta_A\left(Q\right)$ (representing $a_{i+1}$). If the two values match, it computes $\theta_A\left(a_{i+1}\right)$ and updates \textit{Q} with this value.
\end{enumerate}
%
Note that, as usual, all communications must be authenticated. As done for the basic BC PHEMAP case, the exchange of links meant for authentication between A and AS in the setup description are not explicitly reported for the sake of simplicity.


%$x_i \in BC_{A, B}$ allows devices to obtain the counterpart link by means of xor.
%Indeed, being $x_i = a_i \oplus b_i$, device A is able to calculate $b_i = a_i \oplus x_i $, and vice-versa.
%It follows that a valid carnet can be forged only by the verifier since it requires knowledge about undisclosed chains links of two different devices.
%Of course, both devices involved in BC must be authenticated by means of PHEMAP initialization phase.
%
%It is worth noticing that it is not necessary that both device enter in possession of the BC carnet to achieve mutual authentication.
%Indeed, two different schemes are possible.
%In Figures~\ref{fig:bc-aware} and \ref{fig:bc-naware} we sketch interactions among three players, namely two devices A and B and verifier, in three different phases.
%
%In particular, in Figure~\ref{fig:bc-aware}, during the Babel Chain Initialization, device A and B, presumably after a negotiation between them, request to verifier for $t$ BC tickets.
%Depending on the verifier policies about ticket forging, it could accept the request sending them the required BC carnet.
%\msg{Mario}{Ale}{non mi piace sto fatto delle policy detto così... ci dobbiamo ragionare}
%%il verifier potrebbe rifiutarsi nel caso si debba revocare: abbiamo controllo su questo
%During two subsequent verification phases, device A and B completes mutual authentication by exploiting received tickets.
%In the first verification phase, device A calculates the subsequent chain link $a_1$ by $\theta_A\left(a_0\right)$ and, by means of the ticket $x_1$, obtains the $b_1$ link and sends it to device B, which verify if the received link corresponds to $\theta_B\left(b_0\right)$. In case of success, it authenticates device A and analogously prepares $b_2$ by $\theta_B\left(b_1\right)$ and, hence, $a_2$ exploiting the ticket $x_2$ and sends it to device A that, in case of success, authenticates device B.
%The second verification phase differs as device B initiates the authentication request.
%
%Conversely, Figure~\ref{fig:bc-naware} illustrates a slightly different Babel Chain Initialization, which takes into account the solely device A request. Such a procedure requires device A to act as the verifier's proxy for device B because it will not receive BC carnet.
%As a result, the verifier sends a proxy configuration message to B and the requested BC carnet to device A.
%Contrary to the previous BC PHEMAP variant, device B sends to device A its own link.
%In other words, the duty of translating each chain link is of device A.
%
%\subsubsection{BC tickets Removal}
%
%\subsubsection{Security Analysis}\

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{bcphemap_aware.pdf}
	\caption{Proxy-based Babel-chain PHEMAP protocol interactions}
	\label{fig:bc-aware}
\end{figure}


\subsection{Security considerations}
In both BC PHEMAP variants, the only additional information to protect is the BC carnet. 
The carnet may be disclosed, in the basic BC-PHEMAP protocol, either observing the communication channel between the device and AS or by capturing the device. In the Proxy-based variant, the only way to disclose the carnet is to physically tamper with the gateway. In both cases, 
as already mentioned, the carnet does not represent a critical information: each ticket of the carnet is the result of a XOR between two links belonging to two different devices' PUFs, which have never been exchanged before. 

Let us assume that an attacker is able to obtain a carnet and to intercept all messages exchanged among involved parties. For example, let us assume that the attacker observes communications between devices A (provided with the carnet) and B during the basic BC PHEMAP verification phases, and that he/she is able to intercept value ($m_i = a_i \oplus x_i$) sent by A to B. Let us recall that $a_i$ is the first un-exchanged link extracted from A's PUF and $x_i$ is the corresponding carnet ticket. Value $m_i$ may be used by the attacker to easily obtain link $a_i$ by simply computing $m_i \oplus x_i$. However, by then, link $a_i$ has already been spent for authentication purposes and does not provide any useful information to the attacker. 

Other possible attacks, targeting only the Proxy-based BC PHEMAP variant, may try to exploit the role of gateways in the communication. In particular, an attacker may impersonate or capture G to selectively drop or alter exchanged messages. However, since in this case the gateway acts only as a simple message relay between devices A and B, these attacks would not jeopardize the security of the protocol and may be easily detected since they would cause a de-synchronization (due to multiple communication attempts fail), as widely discussed in \cite{barbareschi2018puf}. 
%
