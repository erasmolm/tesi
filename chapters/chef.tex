\chapter{Application Domain and Deployment Scheme}
\label{sec:ch_deploy}
In this chapter a deployment scheme using the Chef framework is provided. The aim of this section is to present a real case scenario of authentication as-a-service. In particular, a service provider may want to realize an IoT application and enforce it by enabling mutual authentication for the infrastructure it created. With reference to a real use case such as a \textit{smart power grid} discussed in \cite{stojmenovic2014fog}.

In this scenario, the service provider manages a power grid, which supplies energy coming from different source, such as wind, solar plants, hydroelectric power plants, etc. In order to realize a load balancing application, edge devices may be smart meters, grid sensors and actuators. The device may switch automatically to a different power source or combine several of them, based on the evaluation of parameters such as demand, price and availability.

Fog node may be data collectors which issues commands to the actuator and collect data from the sensors. As these nodes have mid to low capabilities, the can only provide support for temporary data storage, however, the may filter and reduce data overhead and finally delegate only the strictly necessary to the cloud.

As said, the following entities can be identified:

\begin{itemize}
	\item sensors, smart meters, actuators as edge devices;
	\item gateways, micro-clouds as fog nodes;
	\item central cloud at the highest layer
\end{itemize}

The service provider is responsible for installing terminal nodes, the gateways and the verifier cloud service, which may be either realized ad-hoc by the owner or it can be provided by a third party such as Amazon AWS or Microsoft Azure. On the other hand, the service provider, will be provided with the implementation of the three node entities (verifier, gateway and device) and the framework (Chef) to automate the distribution of the code, device management and resource distribution directly from a workstation.

The main advantage of this solution is that users have to just care about developing a recipe, which is a Ruby based script, in order to declare resources that will be used on the nodes, such as files, scripts, templates, and assign these resources to different role that may be represented by the nodes.

By the continuous integration perspective, whenever the code is updated by the maintainer, it will be automatically built, tested and deployed on the terminal nodes.

In the following section it will be provided an introduction upon the concept of Continuous Integration. Then, more details will be provided about the implementation of the aforementioned deployment scheme using Chef.\\

\begin{figure}[H]
	\centering
	\includegraphics[width=\textwidth]{figures/deploy.pdf}
	\caption{The Chef driven deployment}
	\label{fig:chef_deployment}
\end{figure}

\section{Continuous Integration}
In software engineering, Continuous Integration is the development practice to integrate the work of a team frequently \cite{fowler2006continuous}, at least once a day. Each person integrates his/her modifications of the software as soon as possible, leading to multiple integrations during the day. Whenever the code is modified and integrated, it is built and tested by an automated verifier, in order to detect errors in the first possible stage of development. Indeed, each commit triggers a build job on the mainline repository and it may introduce new errors that could propagate through multiple stage if they are not properly handled. As said, according to the \textit{fail fast} philosophy, each build job is followed by a test job, which aims to find latent errors and solve them as soon as possible, without propagate and merge them with others.

As a result, this approach leads to a significant reduction of the most common integration problems and enforce cohesion within the development team.\\
\\
\\
\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{figures/ci.pdf}
	\caption{The Continuous Integration Development Cycle}
	\label{fig:ci_cycle}
\end{figure}

Regarding this work, the three entities that compose the Extended PHEMAP ecosystem have been implemented in C++ and it is built online using Gitlab pipeline. In particular, each commit triggers a build job which has to expose several artifacts:

\begin{itemize}
	\item the build directory contains the executables for the x64 architectures
	\item the cross directory contains the executables for the arm based architecture, in particular for the Cubieboard Cubietruck
\end{itemize}

In addition, in order to provide the gateway with a basic package with the strictly necessary executables and libraries, the \textit{pufLessPkg.tar} is exposed for the gateway node and it is reachable by means of a permalink that always points to the most recent succeeded build job.\\

\lstinputlisting[language=bash]{code/gitlab-ci.yml}

\section{Chef Automation}

Typically, Chef is comprised of three parts: the workstation, a Chef server, and nodes. In this particular use case, the node is represented by the Cubieboard Cubietruck, running an arm based Ubuntu 18.04 distribution, while the workstation is a simple computer with access to the internet.
The Chef Server it the architectural component which keeps information about the organization managed by Chef and its relative infrastructure. It is possible either to set up a Chef Server on a private machine or sign up for hosted Chef and let Chef manage the server. The setup configuration for the other entities will be discussed in the next paragraph.\\

\begin{figure}[H]
	\centering
	\includegraphics[width=0.9\textwidth]{figures/workstation-server-node.png}
	\caption{Relationship among Chef workstation, Chef server and node, taken from Chef website}
	\label{fig:workstation_server_node}
\end{figure}

The Cubietruck plays the role of the gateway for both BabelChain and Salted PHEMAP. In this example a chef recipe and a template are presented. 

\newpage
\lstinputlisting[language=ruby]{code/recipe.rb}

As shown in the previous code, the recipe accomplishes three specific tasks:

\begin{enumerate}
	\item by defining a \texttt{remote$\_$file}, the Cubietruck is provided with the \textit{pufLessPkg.tar} file, which is obtained through the permalink pointing to the version belonging to the last successful build job;
	\item once the file above is available on the file system, the \texttt{bash} resource is triggered: it directly runs bash commands in order to clean the directory, untar the package and remove it;
	\item the third resource declares a bash script, named \texttt{"run$\_$phemap.sh"}, which is derived from a template called \texttt{"run$\_$phemap.sh.erb"}, and redefines access rights for the script.
\end{enumerate} 

A cookbook template is an Embedded Ruby (ERB) template that is used to dynamically generate static text files. Templates may contain Ruby expressions and statements, and help to manage configuration files. As shown in the following code, the template dynamically defines a variable called \texttt{CT$\_$IP}, which depends on the ip address of the node that is executing the code. This means that, in case of multiple nodes, it is not necessary to define multiple scripts that differ exclusively for the ip address of the node, indeed it is sufficient a single template script that declares a different ip address depending on the specific which is running it. Then, the script exports the library needed by the executable and the launches PHEMAP with the previously declared parameters.

\lstinputlisting[language=ruby]{code/run_phemap.sh.erb}

\subsection{Chef Workstation Configuration}
The workstation is the computer from which the user can edit cookbooks and manage the infrastructure. It uses a git repository in order to keep access keys for the Chef Server, cookbooks and templates. In order to setup the workstation, it is necessary to install the Chef DK by running:

\texttt{curl https://omnitruck.chef.io/install.sh | sudo bash -s -- -P chefdk -c stable -v 2.5.3}\\
Note that this is specific for this particular version of the tool.

The Chef server stores information about the nodes, this information is indexed and is searchable from knife.
Knife is the command-line tool that permits to interact with the Chef Server. It requires to authenticate to the server  by mean of two file: an RSA key pair and a configuration file for knife. The key pair is needed to securely authenticate each request to the server. In particular, the server hold the public key while the private is held by the workstation. The knife configuration file (named \textit{knife.rb}) contains information such as the Chef server URL, the location of the cookbook and the RSA keys.

In order to configure the workstation, it is necessary to create a directory named \texttt{.chef} and put both the key and the configuration file in it. The latter can be generated from the Manage Chef interface as shown in the following figure.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{figures/chef_manage_generate_knife_config-98fa5ee7.png}
	\caption{Chef Manage interface, generate knife config}
	\label{fig:knife_config}
\end{figure}

Again, from the Chef Manage interface, the key is generated by the server for the specific workstation in use.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.6\textwidth]{figures/chef_manage_reset_key-49f8b916.png}
	\caption{Chef Manage interface, generate RSA key}
	\label{fig:knife_key}
\end{figure}

Then, once the cookbook \textit{phemap$\_$cubietruck} has been created, it is necessary to upload it to the server by issuing the command:

\texttt{knife cookbook upload phemap$\_$cubietruck}

Finally, the recipe can be launched by adding it to the run-list belonging to the specific node:

%\texttt{knife bootstrap -y \$CUBIETRUCK$\_$IP --ssh-user erasmo --ssh-password '+' --sudo --use-sudo-password --node-name node1-cubietruck --run-list 'recipe[phemap$\_$cubietruck]'}

\begin{figure}[H]
	\centering
	\includegraphics[width=.95\textwidth]{figures/bootstrap.png}
	\caption{knife bootstrap process}
	\label{fig:knife_bootstrap}
\end{figure}


Whenever the recipe is updated and pushed to the versioning controller, it is possible to update it on the Chef Server by issuing:

\texttt{knife ssh 'name:node1-cubietruck' 'sudo chef-client' --ssh-user erasmo --ssh-password '+' --attribute ipaddress}

Now that the recipe is available on the server it is possible to connect to the node via SSH and launch it on the node. The bootstrap process connects to the node over SSH and enables to configure the nodes without the need to connect to them directly.

\subsection{Chef Client Configuration}
Once that the cookbook is on the Chef server, it is possible to run chef-client on the node. The \texttt{chef-client} command pulls from Chef server the latest cookbooks from the node's run-list and applies the run-list to the node. The knife ssh command enables 

To run chef-client on the node remotely from the workstation, the knife ssh command is needed.
By running knife ssh, it can be either specified node's IP address or a search query that specifies which nodes to connect to. Because the search query syntax supports multiple search patterns and can contain multiple search criteria, knife ssh can run chef-client on multiple nodes at the same time.
