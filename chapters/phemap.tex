\section{PHEMAP protocol}
PHEMAP (Physical Hardware-Enabled Mutual Authentication Protocol), proposed in \cite{barbareschi2018puf}, leverages the concept of PUF chain introduced above to enable the mutual authentication between a device embedding a PUF and a verifier, by extending the basic authentication protocol discussed above. In particular, PHEMAP exploits the chains extracted from a PUF circuit throughout the enrollment phase. The verification phase consists in using chain links to authenticate exchanged messages. It is strictly necessary for the verification phase to maintain both the verifier and the device synchronized on the same link of the chain, hence a procedure of \textit{initialization} is required and it will be discussed later.

\begin{figure}[!htbp]
	\centering
	\includegraphics[width=.65\textwidth]{figures/chain.pdf}
	\caption{PUF Chain}
	\label{fig:phemap_chain}
\end{figure}

\subsection{Notation and technical concepts: chains and sentinels}
Let D be a generic device that embeds the PUF $\theta_D\left(\cdot\right)$.
Let $C$ represent the set of valid challenges for $\theta_D\left(\cdot\right)$ and $R$ be the set of corresponding responses such that $\forall c \in C$, $ \; \exists! \; r \in R : r = \theta_D\left(c\right)$.
%
Without loss of generality, it is assumed that both challenges and responses are represented by $N$-bits binary strings. In case the PUF architecture provides responses with a bit length different from $N$, hash functions could be suitably used to generate corresponding N-bit values.  

It follows that the cardinality of $C$ is $2^N$, while $R$ is a subset of $C$ ($R \subseteq C$). 
The set of all valid CRPs $\left\langle c \in C, r = \theta_D\left(c\right) \right\rangle$ for $\theta_D\left(\cdot\right)$ is indicated as $\phi_D$.

Let us identify with notation $ \theta_D^{t}\left(\cdot\right)$ the \textit{$t$ self-iteration} of the PUF $\theta_D\left(\cdot\right)$, obtained by querying the PUF circuit \textit{t} times by using, at each iteration starting from the second one, the output obtained at the previous iteration as the current input stimulus.

The sequence of strings obtained by self-iterating the PUF $\theta_D\left(\cdot\right)$ for $(M-1)$ times starting from challenge $c_0$ (referred to as the chain \textit{root}) plus $c_0$ constitutes the PUF \textit{chain} $\gamma_{D, c_0, M}$. In particular, the $M$ strings belonging to chain $\gamma_{D, c_0, M}$ are referred to as \textit{links} and identified with notation $l_i$, such that:

\begin{equation}
\begin{aligned}
l_0=c_0, \\
l_1=\theta_D\left(l_0\right), \\
\cdots \\
l_{M-1}=\theta_D\left(l_{M-2}\right) 
\end{aligned}
\end{equation}\\

A chain is said to be \textit{valid} if it does not contain loops, e.g., if each link in the chain appears only once. 
With reference to figure \ref{fig:puf_letters}, all the possible simple paths that can be identified in the graph representation corresponding to the PUF definition represent valid chains.  
For instance, the simple path going from $u$ to $y$ and obtained by self-iterating the PUF for 6 times starting from $u$ represents the valid chain $\gamma_{D, u, 7}$, while the 7 self-iteration of the PUF starting from the same value is not a valid chain, as link $m$ appears twice.

Finally, the definition of chain sentinel is introduced. Given a chain $\gamma_{D, c_0, M}$, a link $\sigma_0 \in \gamma_{D, c_0, M}$ and a positive integer $S$, \textit{sentinels} are the links $\sigma_i \in \gamma_{D, c_0, M}$ that appear in positions multiple of $S$, starting from $\sigma_0$.
$\sigma_0$ is called \textit{sentinel root}, while $S$ is the \textit{sentinel period} and it is fixed a-priori and embedded in both verifier and devices.\\
With regard to the example PUF shown in Figure \ref{fig:puf_letters}, assuming the chain $\gamma_{D, t, 13}$ and setting the sentinel root $\sigma_0$ to $f$ and the period $S$ to 4, the sentinels for the above chain are represented by links: $\left\lbrace f; h; k \right\rbrace$.

The following Table \ref{tab:Notation} summarizes the notation introduced.

\begin{table}[htbp]
\footnotesize
	\centering
		\begin{tabular}{|l|p{10cm}|}
				\hline
		\textbf{Symbol} & \textbf{Description} \\ \hline
		$\theta_D\left(\cdot\right)$ & PUF circuit embedded by device D \\ \hline
		$\phi_D$ & set of all valid CRPs for PUF $\theta_D\left(\cdot\right)$   \\ \hline
		$\theta^{t}\left(\cdot\right)$ & t self-iteration of PUF $\theta_D\left(\cdot\right)$   \\ \hline
		$\gamma_{D, c_0, M}$ & chain obtained from the PUF embedded by device D, with root $c_0$ and length $M$ \\ \hline
		$l_i$ & link on a chain\\ \hline
		$\sigma_i$ & sentinel on a chain \\ \hline
		\end{tabular}
	\caption{Notation summary}
	\label{tab:Notation}
\end{table}
\normalsize
%
\subsection{Assumptions}\label{sec:ph}
The assumptions related to the PUF architectures adopted for the protocol implementation are the following: %
\begin{itemize}
	\item involved PUF circuits behave as \textit{strong PUFs}, so that the exhaustive measurement of all CRPs would result computationally unfeasible;
	\item involved PUFs are \textit{tamper-resistant} and can be accessed only by the hosting device at mission time, while a limited access is provided to the verifier by means of an external interface only during enrollment;
	\item involved PUFs are \textit{reliable}, such that the probability of having a noisy response, due to unpredictable variations caused by changes in the environmental conditions, is sufficiently low.
\end{itemize}
%
% DISCUSSIONE SUL PERCHé SIANO RAGIONEVOLI
Regarding the adoption of strong PUFs, as stated in the previous section, it is still possible to let the protocol unchanged even if weak PUFs are the only available by adopting the solution in figure \ref{fig:weak_puf}.

Moreover, it is feasible to assume the adoption of a reliable PUF. In fact, this property can be intrinsically true for some PUFs circuits, such as the Anderson PUF \cite{anderson2010puf}, or it can be enabled by applying techniques such as error fuzzy extraction, as authors proved in \cite{dodis2004fuzzy}, to significantly improve PUF reliability.\\

\begin{figure}[!htbp]
	\centering
	\includegraphics[width=.9\textwidth]{figures/fuzzy.pdf}
	\caption{A general Fuzzy Extractor implementation. To the left, helper data and key are extracted during the enrollment phase. To the right, from a noisy PUF response, the same key is reproduced.}
	\label{fig:fuzzy}
\end{figure}

Furthermore, the tamper-resistance property is intrinsically maintained by PUF circuits and direct access  to the inner logic of the device (i.e. registry scan chain) can be blocked by disabling any external interface before issuing the device. For example, the PUF circuit could be embedded on an FPGA, hence the producer could design two different bitstreams: one for the normal operations with no external access to the PUF circuit, and the other for the enrollment phase with fully enabled access to the PUF circuitry.
To sum up, the assumptions made for PHEMAP are reasonable and do not hinder the application of the protocol to a real scenario.
%
In addition, according to the previous discussion upon PUF-based authentication, some assumptions regarding the capabilities of the device have to be remarked:
\begin{itemize}
    \item the enrollment phase is executed in a trusted environment and CRPs are stored in a secure database;
    \item in each device there is a secure tamper-proof perimeter, in order to avoid sensitive data alteration at run-time by malicious users;
    \item each entity is equipped with a pseudo-random number generator.
\end{itemize}
%
According to the assumptions above, PHEMAP was designed in order to keep the resource consumption on the devices as low as possible, and therefore it only requires devices to embed a register \textit{Q} of N bits to store the last consumed link, which is the only hardware status information maintained on the device side.
Note that PHEMAP requires \textit{Q} to be included in a tamper-proof secure perimeter, such that it cannot be altered by a malicious user during the protocol execution.
%
\subsection{Enrollment}
Basically, in order to keep the need for storage capabilities as low as possible for the device, instead of using a large set of random \textit{distinct} challenges to enroll the PUF, the entity responsible for the enrollment randomly selects \textit{one} random challenge $c_0$ and uses it to generate a PUF chain $\gamma_{D, c_0, M}$ of maximum length $M$. This operation is repeated $K$ times to generate a set $\Gamma_D$ of $K$ distinct chains, such that each link appears only once, hence chain length $M$ is influenced by the PUF circuit capability to always generate new distinct links.
%
% ALGORITMO CHAIN-MAKER
The algorithm proposed in \cite{barbareschi2018puf} is reported below. Basically, starting from a set of previously extracted chains, given the PUF function and a maximum length $M_{max}$ for the chains as input, it generates a new chain of length at the most equal to $M_{max}$, starting from a random challenge $c_0$.\\
%
\begin{algorithm}[H]
\caption{Chain generation algorithm}\label{alg:chainmaker}
\renewcommand{\algorithmicrequire}{\textbf{Input:}}
\renewcommand{\algorithmicensure}{\textbf{Output:}}
\begin{algorithmic}
\Require{$\Gamma_D,M_{max},\theta_D$}
\Ensure{$\gamma_{D, c_0, M} \notin \Gamma_D, M \leq M_{max}$ }
\State $M\gets 0$
\Repeat
\State $c_0\gets rand$
\Until $c_0 \in \Gamma_D$
\State $M \gets M+1$
\State $initialize\ \gamma_{D, c_0, M}$
\State $x \gets c_0$
\While{$M<M_{max}$ \textbf{and} $\theta_D(x) \notin \Gamma_D$}
\State $x \gets \theta_D(x)$
\State $append\ x\ to\ \gamma_{D, c_0, M}$
\State $M \gets M+1$
\EndWhile
\State \textbf{end while}\\
\end{algorithmic}
\end{algorithm}
%
As stated before, in case of a weak PUF, it is still possible to preserve the PUF mathematical unclonability. In particular, the weak PUF can be combined with a cipher. During enrollment, the cipher key is generated by the PUF circuit. Then the verifier, instead of applying challenges to the PUF interface, submits them to the the cipher input. It must be emphasized that the key secret and it is impossible to recover it in order to reproduce the behavior of the PUF.
%
\subsection{Initialization}
The verification phase is based on the classic challenge-response paradigm, in which an authenticator sends a challenge (a link $l_i$ from an enrolled chain) to a supplicant and compares the received response (chain link $l_{i+1}$).

It must be noted that the verifier can easily extract a challenge from the enrolled chains and verify the respective response from the device by inspecting the following links on the chain. On the contrary, given that devices have poor storage a compute capabilities and cannot store the enrolled chains nor verify if a link has never been used before, this operation is not trivial for a device node.

In order to solve this issue, verification relies upon the \textit{initialization} step, which not only provides link synchronization between verifier and device, but it also enables the first mutual authentication between the two entities.

The initialization procedure is carried out at device issue time or in other cases including de-synchronization (due to loss or compromise of authentication messages) and chain link exhaustion.

It is always triggered by the verifier and it is made up of three messages built through the manipulation of a number of consecutive links of a chain using random nonces and XOR functions.

In order to prevent an attacker from recording the links exchanged (in clear-text) during previous verification steps and carrying out a man-in-the-middle attack to accomplish a successful initialization, the protocol leverages the concept of \textit{chain sentinel} introduced above.
Basically, said $S$ the sentinel period, fixed a-priori and embedded in both verifier and devices, in order to build protocol messages composing the initialization phase, the knowledge of at least $S$ consecutive links is necessary. In contrast to this, no more than $S - 1$ consecutive links are ever exposed. 
To ensure this property, sentinel links are skipped during verification and are never exchanged between the two parties. On the device side, this can be easily accomplished by using a simple modulo-S counter, which for the device must be place within the secure perimeter.

As stated in \cite{barbareschi2018puf}, the initialization procedure is made up of four phases, which will be remarked in the following.

% FORMULE MATEMATICHE
\textbf{Phase 1:} The verifier generates a random nonce \textit{n} and sends the following message to the device.

\begin{equation}
init_1 = \left\lbrace l_i,\left( \bigoplus_{j=0}^{S-3}l_{i+1+j} \right) \oplus\ n, l_{i+S-1} \oplus n \right\rbrace= \lbrace l_i,v_1,_2 \rbrace 
\end{equation}

\textbf{Phase 2:} the device verifies that:

\begin{equation}
\bigoplus_{j=0}^{S-2}\theta_D^{(j+1)}(l_i) == v_1\oplus v_2
\end{equation}


And, whether the comparison succeeds, it generates another random nonce $m$ and builds the following message.

\begin{equation}
init_2=\left\lbrace \theta_D^{(S)}(l_i)\oplus m,\theta_D^{S+1}(l_i)\oplus m \right\rbrace=\left\lbrace d_1,d_2 \right\rbrace 
\end{equation}\\
Furthermore, the device stores $d_2$ within the secure register \textit{Q} and sends $init_2$ to the verifier.

\textbf{Phase 3:} the verifier computes:\\
\begin{equation}
l_{i+S}\oplus l_{i+S+1} == d_1 \oplus d_2
\end{equation}

At comparison success, the device is authenticated to the verifier, the latter sends the last message $init_3$ to the device.

\begin{equation}
init_3=\left\lbrace l_i,l_{i+S+2}\oplus m \right\rbrace=\left\lbrace l_1,v_3 \right\rbrace
\end{equation}

Finally, the device calculates:

\begin{equation}
\theta_D^{(S+1)}(l_i) \oplus \theta_D^{(S+2)}(l_i) == v_3 \oplus d_2
\end{equation}

\subsection{Verification}
Both the verifier and the device can start a verification procedure. This operation is substantially an exchange of two consecutive link belonging to an enrolled PUF chain extracted from the device.\\
Let us assume that $\gamma_{D, l_0, M}$ is currently being used by the verifier, and that the last consumed link is $l_{i-1}$, stored on the device's secure register \textit{Q}.

When the verifier wants to communicate with device D, the following steps are executed:
%
\begin{enumerate}
	\item the verifier picks the link immediately following $l_{i-1}$ in $\gamma_{D, l_0, M}$, namely $l_i$, and sends $m_1 = l_i$ to the device;
	\item the device compares $m_1$ with the value $\theta_D\left(Q\right)$, i.e., with $\theta_D\left(l_{i-1}\right)$. 
	If the two values match, D authenticates the verifier. Afterward, the device computes $m_2 = \theta_D
\left(m_1\right)$ (corresponding to $\theta_D\left(l_i\right) = l_{i+1}$), stores this value in its register \textit{Q}, and sends it to the verifier;
	\item the verifier compares $m_2$ with the link $l_{i+1}$ immediately following $l_i$ in the chain. If the two values match, the verifier authenticates the device.

\end{enumerate}
At the end of verification, both parties are synchronized on value $l_{i+1}$.

When the device is the initiator of a verification step, things work pretty similarly: the device challenges the verifier with the value $\theta_D\left(l_{i-1}\right)$, and the verifier checks whether this value matches the link immediately following $l_{i-1}$ in $\gamma_{D, l_0, M}$, namely $l_i$. Then, the verifier sends $l_{i+1}$ to the device that, in turn, checks whether it corresponds to $\theta_D\left(l_i\right)$.

It is worth noting that, although links are exchanged in clear-text during verification, each link is used only once and chains are progressively invalidated as all respective links are consumed, therefore replay and man-in-the-middle attacks are not possible. Furthermore, both the entities have an S-modulo counter that keeps track of sentinel nodes. This counter is reset after every initialization step. For how it concerns to the device, the sentinel counter resides within the secure perimeter together with the Q register, that keep track of the last exchanged link.\\
Next section reports an example of the PHEMAP, in order to summarize how it works.

%
% ESEMPIO
\subsubsection{Example}
Let us consider the chain $\gamma_{D,t,13}$ extracted from the PUF reported in  Figure~\ref{fig:puf_letters}, namely: 

$\gamma_{D,t,13} = \left\langle \text{t}, \text{n}, \text{f}, \text{q}, \text{z}, \text{r}, \text{h}, \text{x}, \text{c}, \text{s}, \text{k}, \text{g}, \text{l} \right\rangle$ \\ 
\noindent and let us assume that the sentinel period is set to $S=4$.

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{phemap_example.pdf}
	\caption{An example of initialization and verification in PHEMAP}
	\label{fig:phemap_example}
\end{figure}
Figure \ref{fig:phemap_example} reports the interactions between the verifier and the device during initialization and verification.
As shown, during initialization, links from \texttt{t} to \texttt{h} are consumed to generate protocol messages (more in general, $S + 3$ consecutive links are involved in initialization). 
As anticipated, these links are combined together and with suitable nonces by means of XOR functions. 
Moreover, as shown, the device uses an internal register $Q$ to store the last consumed link, which after initialization is $\theta_D^6{\left(t\right)}$, i.e., \texttt{h}.

By definition, the link \texttt{x}, immediately following \texttt{h} in the chain, is chosen as the \textit{sentinel root}, and the subsequent links appearing in positions multiple of $S = 4$ from \texttt{x} (only \texttt{g} in the example) are marked as sentinels.
 
Once the initialization has been successfully completed, both the verifier and the device are synchronized upon link \texttt{h} and are able to initiate a verification step. 
In the first verification block reported in Figure~\ref{fig:phemap_example}, the initiator is the verifier. It picks the next link to consume from the chain, namely \texttt{c} (\texttt{x} is skipped as it is a sentinel), and uses it as the challenge to send to the device. The device, which knows as well that the value $\theta_D{\left(Q\right)}$ corresponds to a sentinel (thanks to the embedded modulo-S counter), computes $\theta_D^2{\left(Q\right)}$ and compares it with the link received from the verifier.
If the two values match, the device stores locally and then sends to the verifier the value $\theta_D\left(c\right)$. Finally, the verifier checks the correspondence between the received value and the link immediately following \texttt{c} in the chain, namely \texttt{s}.

In the second verification block reported in figure, the initiator is the device. It computes $\theta_D\left(Q\right)$, updates register \textit{Q} with this value and sends it as the challenge for the verifier. The verifier simply checks whether the received value corresponds to link \texttt{k} and, in case of success, returns the next link of the chain, namely \texttt{l} (\texttt{g} is skipped as it is a sentinel). The device, similarly to the previous case, computes $\theta_D^2\left(Q\right)$ and compares it with the received link. If the two values match, the verifier is considered authenticated and \textit{Q} is updated with $\theta_D^2\left(Q\right)$.
%
\subsection{De-synchronization and chain exhaustion}
As described before, chains are built by self iterating the PUF function embedded within the device D. Bearing in mind that chains must not contain loops, it is straightforward that the chain generation process stops when a link already belonging to $\gamma_D$ is found. Hence, this condition determines the theoretical bound for the length $M$ of the chain. On the other hand, it is possible to set $M$ to a value suitable for a real case scenario, for instance the maximum time spent for the enrollment phase or the maximum storage for the enrolled chains within the verifier secure database.

Anyway, chains are finite collections of links, and therefore their possible exhaustion must be taken into account, since the authentication relies upon the exchange of links. In general, the verifier can always trigger a new chain initialization to cope with the chain exhaustion problem and re-synchronize with the device. As a result, this procedure introduces overhead, since initialization phase involves longer messages to be exchanged, moreover $M$ becomes a critical parameter for the protocol performances.
%
De-synchronization is another problem the may occur at run-time, both during initialization or verification. In essence, de-synchronization occurs when device and verifier don't share the same knowledge about the current link to transmit. There may be different causes for this problem to occur, such as device tampering, communication channel fault, bad PUF response, verifier or device fault. Anyway, de-synchronization is resolved by eventually triggering a new initialization procedure.
%
\subsubsection{De-synchronization during initialization}
The initialization phase is made up of three messages ($init_1,init_2,init_3$), and de-synchronization may occur during the transmission of each message.

If $init_1$ is lost, because this message is sent by the verifier to the device, the first simply won't receive any response while the latter will remain unaware. This case is addressed by issuing a new $init_1$, with a new link. Similarly to the previous case, if $init_2$ is lost, the verifier won't receive any message and it will simply re-trigger the initialization procedure. In both cases neither the verifier will authenticate the device nor vice versa.

When message $init_3$ sent by the verifier to the device is lost, the verifier has authenticated the device, but the device has not yet authenticated the verifier. To address this case, there are two possible solutions:
\begin{itemize}
	\item by introducing a new \textit{ACK message}, sent by the device to the verifier to confirm the authentication by the device, and setting a timer at the verifier to restart the initialization when the \textit{ACK message} is not received on time;
	\item by setting a timer at the device to trigger the transmission of a \textit{NACK message} when the $init_3$ message is not received on time. 	
\end{itemize}
For what concerns about faulty PUF responses, this case may occur during phase 2 or phase 4.
\begin{itemize}
	\item In the first case, the device applies the PUF function once for the verification of the $init_1$ message and then for the generation the $init_2 message$. When the PUF fails during the first iteration, the device is unable to authenticate the $init_1$ message from the verifier, so it won't carry on the initialization and the verifier will restart the initialization after the timeout. When a PUF fail occurs during the second iteration, the device will issue a wrong $init_2$ message, so the verifier won't authenticate it, once received, and it will trigger another initialization.
	\item In the second case, the device applies the PUF to verify $init_3$, if the PUF fails during this operation it won't authenticate the verifier and the case can be reconducted to de-synchronization when $init_3$ message is lost.
\end{itemize}
%
\subsubsection{De-synchronization during verification}
As discussed above, the verification procedure involves the transmission of two messages, in order to prove that both the verifier and the device share the same knowledge about the enrolled chain. When the procedure is started by the verifier, there are two cases of de-synchronization:
\begin{itemize}
	\item the message sent by the verifier is lost. A result, the verifier will be synchronized on $l_{i+1}$ and the device one $l_i$;
	\item the response sent by the device is lost. The verifier is synchronized on $l_{i+1}$ and the device on $l_i+2$
\end{itemize}
%
The verifier is unable to distinguish between the two cases above, as it won't receive a response from the device anyway. Both the cases can be addressed using a timer at the verifier, in order to trigger a new verification attempt up to a maximum number of attempts. As stated before, a chain link cannot be exchanged more than once, so the verifier issues a new attempt sending the successive non-sentinel PUF link on the chain.

In case of de-synchronization caused by a wrong PUF response there will be two different cases.
\begin{itemize}
	\item The PUF fail during the verification of the link $l_i$ sent by the verifier. In this case no response will be sent by the device, resulting in a new verification attempt after a timeout.
	\item When the error occurs during the generation of device reply with the $l_{i+1}$ link. As a result, the device could authenticate the verifier, but the reply will be rejected by the verifier. It must be noted that the device will store a wrong link within its secure register, as a consequence, this will compromise not only the current verification step, but also the successive ones. However, the verifier is always able to detect this kind of situation and eventually trigger a new initialization.
\end{itemize}
When the verification is started by the device, the solution is similar to the previous one. Assuming to provide the device with a timer and that the device sent a verification message containing the link $\theta_D(l_{i})$, if the device does not receive any reply before the timeout, it will send a new request with $\theta_D^2(l_{i})$ and then $\theta_D^3(l_{i})$ for a maximum number of attempts, after which the device will explicitly request a new initialization to the verifier. Moreover, the verifier needs to be able to check consecutive links of the chain after $l_{i+1}$, for a maximum number of links.
%
In this case, PUF unstable responses lead to two different situations.
\begin{itemize}
	\item The device may extract a wrong value during the generation of the first link and store it within its secure register. In this case, the verifier will reject the message and re-initialize the device.
	\item The PUF may fail during the verification of the verifier reply. In this case, the device won't authenticate the verifier and no message will be sent as reply nor any value will be stored within the secure register.
\end{itemize}
% -----------------------------------------------------------------------------------------------
\subsection{PHEMAP Analysis}
%
\subsubsection{Security Considerations}
The main aim of the PHEMAP protocol is to ensure mutual authentication between two communicating entities and prevent \textit{man-in-the-middle} attacks that aims to impersonate either a legitimate device or the verifier.
In case of a malicious device, it is straightforward that the unclonability of the PUF ensures that the response expected by the verifier cannot be reproduced by a different device. On the contrary, ensuring that an attacker cannot impersonate the verifier and be authenticated by the device is not trivial.
As stated in \cite{barbareschi2018puf}, the impossibility for a malicious verifier to be authenticated by the devices can be demonstrated through the subsequent theorems.\\

\textbf{Theorem 1. }\textit{Let D be a legitimate device embedding a PUF $\theta_D(\cdot)$, and let V be a malicious third-party that wants to impersonate the verifier. Even if V has been able to intercept all past exchanged messages between the device D and the legitimate verifier, it is not able to perform the initialization phase to get synchronized with D.}\\

\textbf{Proof. }A malicious verifier V, during initialization, can exploit previous communications between the device and the legitimate verifier, by replaying old messages or by forging new messages from previously exchanged links. Two cases must be distinguished:\\

\textbf{Case A. }\textit{V replays initialization messages used in previous initialization attempts. } Once a successful initialization procedure is completed, the messages $init_1$, $init_2$, $init_3$, created using random nonces $n$ and $m$ have been exchanged. If the malicious verifier V replays $init_1$ to the device D, it will succeed, since D does not store any information about past initializations excepted the last exchanged link, which however is not related to any chain from the device point of view.

Afterwards, the device D picks up a new random nonce $m'$ such that $m' \neq m$ and builds message $init'_2$ that contains the same links as for $init_2$, so it will be different anyway because $init'_2$ relies upon the XOR-ing with the nounce $m'$. Therefore, V will not be able to build the proper message $init'_3$ suited for $init_2$.\\

\textbf{Case B. }\textit{V tries to forge initialization messages by exploiting past exchanged plain text links or links stored in the device memory. }Assuming that V has been able to collect a number of subsequent links, during the previous initializations and verifications. Because of the omission of sentinels in the exchanged messages, the malicious verifier V can collect a maximum of $S-1$ subsequent links. On the other hand, a number of $S$ subsequent links are necessary for the initialization to succeed. As a consequence, V will not be able to complete a successful initialization phase.

In addition, assuming that V is able to steal the content of the device memory after the initialization and, with reference to figure \ref{fig:phemap_example}, the last stored value is $h = \theta_D^6(l_a)$. The malicious verifier V is able to extract $v_3 = m \oplus h$ from message $init_3$, and so it is able to compute $m$ with a XOR operation. By knowing $m and init_2$, V is able to compute $z$ and $r$, for a total of 3 subsequent links. However, as for the previous case, V does not have a sufficient number of consecutive links needed for a successful initialization.\\

\textbf{Theorem 2. }\textit{Let D be a legitimate device embedding a PUF $\theta_D(\cdot)$, and let V be a malicious third-party that wants to impersonate the verifier. Even if V has been able to intercept all past exchanged messages between the device D and the legitimate verifier, it is not able to get authenticated by the device}\\

\textbf{Proof. }Assuming that D has been previously initialized by the legitimate verifier and synchronized in the link $l_i \in \gamma$. The untrusted verifier V may try to start a verification step or it can intercept an authentication request from the device and impersonate the legitimate verifier. Two cases must be distinguished:\\

\textbf{Case A. }\textit{V is the initiator of the verification. }Bearing in mind that D is synchronized on $l_i$, it is expecting $\theta_D(l_i)$ as a new challenge. On the other hand, $l_{i+1}$ has never been exchanged before, nor it can be computed by V. Therefore, this attempt for authentication will fail.\\

\textbf{Case B. }\textit{D is the initiator of the verification. }In this case, D sends a new challenge $\theta_D(l_i)=l_{i+1}$ to V and expects the response $\theta_D^2(l_i)=l_{i+2}$. Obviously, V is not able to generate any subsequent link to $l_i$, so it cannot complete the verification procedure.\\

Moreover, in order to prevent \textit{man-in-the-middle} attacks, PHEMAP was designed to avoid the leakage of PUF links. An attacker who is able to read the content of the device memory cannot infer any useful information about the PUF circuit and the subsequent links, since a link (or a link XOR-ed with a nonce) is stored in a secure register. In addition, new links are exchanged only if strictly necessary, in order to exposes the minimal amount of sensitive information, that may be used to carry out a modeling attack against the adopted PUF.\\
Finally, it must be remarked that the protocol is quite resilient against Differential Power Analysis (DPA) attacks with challenges and responses with a proper size in bits (i.e 128 bits or more). On the contrary, the adoption of an AES cipher to mask links would make the initialization procedure sensitive to DPA attacks, as AES relies upon XOR operations on short binary blocks.
%
\subsubsection{Performance Analysis}
\label{sec:phemap_performance}
In \cite{barbareschi2018puf}, PHEMAP was exhaustively analyzed for what concerns performances, the overhead and the resource usage of the protocol. For what concerns about efficiency, it can be computed as the ratio between the number of links actually useful for authentication and the total number of enrolled links. It must be remarked the part if the PUF links are skipped since they are sentinels or for re-initializations procedures.
Bearing in mind that chains are limited and thus are subject to exhaustion, the number of wasted links must be kept at its minimum. The sentinel period $S$ determines the number of links that are actually used for authentication. With $M$ being the number of all the enrolled links from the device the number of exchanged links for authentication is:

\begin{equation}
	\begin{aligned}
		M_A(M,S)=M-\left\lfloor \frac{M}{S} \right\rfloor - (S - 3)
	\end{aligned}
\end{equation}\\
%
The equation above means that, ideally with no de-synchronization, the number of available links is the number of all enrolled links subtracted the number of links needed for the first initialization and all the sentinels. Moreover, the maximum of this function is reached for $S=\sqrt{M}$. By substituting and by $M$, the total number of enrolled links, the maximum efficiency for $I$ re-initializations occurred can be obtained.

\begin{equation}
	\begin{aligned}
		\eta(M,I)= \frac{M-\left\lfloor \frac{M}{\sqrt{M}} \right\rfloor - I \times (\sqrt{M} - 3)}{M}   
	\end{aligned}
\end{equation}

It is straightforward that the above equation depends a lot on the packet loss probability of the underlying network, as bad network performances increase the number of re-initialization issued in order to cope with the resulting de-synchronization. It can be noted in Figure \ref{fig:available_links} that the efficiency $\eta(M,I)$ increases, tending to 1, with greater values of $M$, while a greater number of initializations $I$ decreases the overall efficiency. Luckily, in a real use case scenario, the verifier is provided with a great storage capability, thus choosing great values of $M$ is feasible.\\

\begin{figure}[!h]
	\centering
	\includegraphics[width=.95\textwidth]{available_links.pdf}
	\caption{Efficiency values depending in the number of enrolled links $M$ and re-initializations}
	\label{fig:available_links}
\end{figure}
%
Regarding the overhead introduced by the protocol, with reference to the basic PUF-based authentication mechanism introduced in \ref{sec:puf_auth}, PHEMAP introduced additional hardware for the device and additional time due to the initialization phase and the additional link exchanged during verification step for the mutual authentication, this additional link involves the computation of a new PUF response.

About the hardware, a part from the PUF circuit, the device needs a volatile memory in a secure perimeter for the $Q$ register, an S-modulo counter in order to skip sentinels, and random number generator necessary for random nonces $m$ and $n$.
%
About the verifier, PHEMAP requires the storage of chain links. This could be expensive from the resource point of view, on the other hand the verifier is supposed to have great storage capabilities. Moreover, chain links can be stored in a hash table, in order to require a relatively low computational effort.
%
As it was discussed in \cite{barbareschi2018puf}, the measured latency due to re-initialization attempts may vary according to the sentinel period $S$ and to the packet loss probability of the underlying network. For the latter parameter, latency has an hyperbolic behavior that tends to infinite with greater values of packet loss rate. However, as stated in \cite{mansfield2010computer}, a packet loss rate greater than 0.25 is unacceptable for most part of the applications, thus it makes no sense to consider a greater packet loss probability.

Regarding the sentinel period $S$, it influences the complexity of the operations involved into the authentication procedure, in particular it increases the number of links necessary for the generation of the initialization messages $init_1,init_2,init_3$. As proved in \cite{barbareschi2018puf}, latency exhibits a linear behavior in function of the sentinel period. It must be noted that the time required by the device to complete its initialization steps is less than the one spent by the verifier. This is due to the fact that the verifier has to access the file system in order to extract a PUF link, while the device has only to compute a PUF response, which obviously requires less time compared to accessing the file system.
%