\chapter{Introduction}
\label{sec:intro}

%remainder


The Internet of Things (IoT) is one of the most dynamic and promising area in the current IT landscape. 
According to the forecasts, an exponential growth of IoT devices is expected in the next years (up to 25 billion units will be active in 2021 according to Gartner \cite{Gartner2017}), with a consequent explosion of produced and processed data.
In order to be able to manage the huge amount of generated data while not sacrificing the processing efficiency in IoT applications with low latency requirements (e.g., surveillance video stream processing, vehicular network traffic data processing, etc.), the combination of cloud capabilities and Edge computing features (referred to as the \textit{Cloud-Edges paradigm}) has recently emerged as a valuable solution to ensure fast and reliable computation, high scalability, location awareness, and energy efficiency.

The Cloud-Edges (CE) paradigm distributes part of the intelligence, processing power and communication capabilities among the so-called \textit{Edge devices}, represented by highly-heterogeneous appliances (ranging from PCs to smartphones, from smart devices to automation controllers, etc.) which are as close as possible to the component, device, application or human that produces the data being processed, while heavy computation tasks are carried out in cloud data centers.

IoT systems developed according to the CE paradigm are typically organized into three architectural layers:
 
\begin{enumerate}
	\item the upper layer is represented by the cloud services used for heavy data processing and responsible for providing the IoT system management and control functionalities to end-users;
	\item in the middle, a set of (possibly heterogeneous) devices, referred to as \textit{gateway nodes}, act as logic intermediaries between the terminal nodes in a logic sub-network and the cloud services. These devices are still at the edge of the network, but are typically provided with higher computation capabilities compared to terminal nodes and are usually responsible for the management of a subset of them.
	\item the lower layer includes a multitude of heterogeneous (edge) IoT devices (called \textit{terminal nodes} hereafter) that generate the data to process (either directly or by operating a first filtering/aggregation of raw data produced by other simpler devices, such as sensors) 
	and possibly interact with the physical environment;
\end{enumerate}

\begin{figure}[!htbp]
	\centering
	\includegraphics[width=.9\textwidth]{figures/arch_iot.pdf}
	\caption{Cloud-Edges Architecture}
	\label{fig:arch_iot}
\end{figure}

Depending on the specific implemented application, terminal nodes may need to communicate directly with one another, in addition to being able to exchange information with their reference gateway. 
Similarly, gateways may need to coordinate with one another, in addition to exchanging messages and data with the service layer.
%
Gateway nodes are typically provided with higher computation capabilities compared to terminal nodes and  they correspond to fog nodes of the Fog Computing paradigm. Whether they are logically positioned at the edge of a local area network, they can be seen in general as more powerful Edge nodes acting as collectors and gateways toward the cloud services to relieve the burden of terminal nodes.

The term \textit{fog computing}, coined by Cisco in 2014, is used to identify a computing paradigm that extends the cloud model by distributing computing, networking, and storage resources on the edges of a network, closer to the devices that generate the data to process. 
Based on this paradigm, a large part of data processing is carried out on the edge devices (including switches, routers, industrial controllers, embedded servers, video surveillance cameras, etc.) rather than in cloud data centres. This ensures a reduction of data transmission overhead and allows for faster and more reliable elaboration, while also having a positive impact on costs and energy consumption.

The adoption of fog computing appears particularly suited in the context of IoT applications with low-latency requirements (e.g., surveillance video stream processing, vehicular network traffic data processing, etc.), where it has been proved to outperform classic cloud computing approaches \cite{Sarkar2015fogIoT}. 
The characteristics of such an architecture raise critical security and privacy challenges, mainly due to the lack of a centralized control and to the exposure of user sensitive data.
%~\cite{Mukherjee2017secissues,Khan2017fogsecurity}. 
%
Unfortunately, the relatively small computing resources of fog nodes, along with the typical constraints of terminal IoT devices, make it difficult to implement sophisticated security solutions, thus exacerbating the complexity of the problem.
As outlined in~\cite{stojmenovic2014fog,stojmenovic2016overview}, 
CE systems are particularly prone to data tampering and spoofing attacks targeting gateways and terminal nodes with the aim of stealing sensitive data or of compromising and/or taking control over the communications.
In this scenario, authentication plays a fundamental role to prevent unauthorized devices from becoming part of the network and to establish trust among involved nodes.

As a matter-of-fact, in the architecture discussed above, a \textit{mutual} authentication mechanism should be provided both at the terminal node level, to enable two terminal devices to mutually verify their identities when communicating with each other, and between terminal nodes and respective gateways.

In order to enable gateway nodes to validate whether terminal nodes requesting their services are genuine and, at the same time, to allow the latter to verify whether the intended gateway nodes are legitimate, even communications among terminal nodes should be secured. Unfortunately, due to the heterogeneous nature of involved terminal nodes and to their typically constrained resources, implementing traditional encryption-based authentication protocols may be not viable, and different solutions must be devised. Moreover, several security specifications, such as the one proposed by the Trusted Computing Group (TCG), do not adopt an integrated security approach suited for the Cloud-Edges paradigm. Indeed many proposed strategies, such as Remote Attestation introduced by TCG, rely upon cryptography, which turns out to be a critical design choice. First of all, cryptographic primitives requires a computational effort that is hard to meet for very resource-constrained devices, mostly because they are provided with discontinuous power supply (i.e. batteries). Moreover, cryptographic schemes rely upon keys, meant to be secret and, hence, they must be kept secret.
Bearing in mind those previous considerations, realizing dependable and performing CE architectures ad-hoc solutions must be employed.

This work has a twofold aim: (i) exploiting Physically Unclonable Funtions (PUFs) to provide security by means of hardware primitives, immune to physical tampering and probing, enabling hardware authentication; (ii) introducing different software protocols, which make use of PUFs, to conjugate CE tiered and distributed architectures. 

As for PUFs, they have been recently recognized as a powerful means to implement relatively lightweight hardware-based security primitives~\cite{pappu2002physical}, and have been successfully adopted to achieve identification and authentication in resource-constrained embedded devices~\cite{suh2007physical,frikken2009robust,rostami2014robust,barbareschi2018puf}. 
A PUF is an integrated circuit provided with unique characteristics conferred by the random imperfections introduced during the manufacturing process. These imperfections make the response of the PUF to specific stimuli unique, not predictable and unclonable, and enable the adoption of PUFs to identify, uniquely, the devices that embed them. In a traditional PUF-based authentication scheme, a device equipped with a PUF is first \textit{enrolled} by a \textit{verifier} entity that collects, offline and in a protected environment, a number of PUF input/output pairs from the device. These pairs are then used, according to a challenge/response protocol, to verify the identity of the device in subsequent authentication requests.

In \cite{barbareschi2018puf}, Barbareschi et al. presented a PUF-based mutual authentication protocol (PHEMAP) that extends the traditional PUF-based authentication scheme in order to ensure mutual authentication in a one-to-many communication scenario, where multiple devices provided with a PUF circuit are connected to a sink node acting as the verifier and the controller of the network.

This work is going to leverage and extend previous works to propose a mutual authentication scheme that enables to establish mutual authentication among the edge devices belonging to a CE-based IoT system. 
The proposed scheme devises the preliminary enrollment of the terminal nodes' PUFs by an \textit{authentication service} hosted in the cloud, and relies on the partial delegation of verification capabilities to the gateways. 
In particular, the scheme includes both a protocol (i.e., \textit{salted PHEMAP}) that enables to achieve mutual authentication between a terminal node equipped with a PUF and a gateway node elected as the respective \textit{local verifier}, and a protocol (i.e., \textit{babel-chain PHEMAP}) that enables to achieve mutual authentication among terminal nodes provided with a PUF, either belonging to the same logic sub-network or not.

In a traditional PUF-based authentication scheme, the PUF circuit embedded on a device is first queried by a verifier to collect, offline and in a protected environment, a number of PUF input/output pairs. These pairs are then used, according to a challenge/response protocol, to verify the identity of the device in subsequent authentication requests.
In order to enable mutual authentication, both parties should share a set of challenge-response pairs and should be able to keep track of the challenges that are consumed over time, in order to thwart common attacks such as replay attacks and man-in-the-middle attacks. 
In the considered scenario, where both fog and terminal nodes are provided with limited resources, mutual authentication is hard to achieve, unless a substantial improvement of the basic authentication scheme is introduced.

In order to evaluate the effectiveness and the efficiency of the proposed scheme, both simulation experiments and tests on a real set-up has been conducted, represented by an STM32F7 and an STM32L4 implementing two terminal nodes, a  Cubieboard Cubietruck acting as a gateway, and an Azure Virtual Machine representing the verifier.

Furthermore, in order to provide a distributed authentication service, an automated deployment scheme has been proposed using the Chef Automate Framework. The latter provides the capability to manage a distributed environment of heterogeneous nodes, by defining resources and roles through a simple script file named \textit{recipe}, and distribute these resources to specific nodes from a remote terminal, namely the \textit{Chef Workstation}.

The proposed solution is suitable for a real case scenario, such as smart metering, in which an IoT service provider can enable mutual authentication \textit{as-a-service} for its IoT application, providing him with the software for the nodes (verifier, gateways, devices) and the framework to automatically distribute it with ease by editing one or more recipes within a \textit{cookbook}.

The remainder of this work is organized as follows. Chapter \ref{sec:ch_background} provides an overview of Cloud-Edges security issues and existing hardware solutions for authentication. Chapter \ref{sec:ch_pufs} presents PHEMAP and its extension ePHEMAP. Chapter \ref{sec:ch_perf} analyzes the performance of ePHEMAP. Chapter \ref{sec:ch_deploy} describes a CI-based solution for automated deployment of the ePHEMAP application. Conclusions and future works are reported in Chapter \ref{sec:ch_conclusion}.