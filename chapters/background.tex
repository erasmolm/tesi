\chapter{Background on CE and Authentication Protocols}
\label{sec:ch_background}
\section{Cloud-Edge IoT Systems Security issues}
The concept of Edge Computing is often used interchangeably with that of \textit{Fog Computing}, which has been introduced by Bonomi \cite{Bonomi2012fog} and leveraged by Cisco Systems Inc. in their \textit{Cisco IOx} framework \cite{ciscoIoX}. It actually refers to a specific, standardized way of implementing Edge Computing, namely by pushing the intelligence to the local area network level, in a smart router/gateway or in a data hub on a smart device. 
Both Edge and Fog Computing paradigms were born to address the requirements of low latency, location awareness and energy and bandwidth saving of many IoT applications, including smart grids, smart traffic lights, smart buildings and video analytics \cite{stojmenovic2015overview, liu2018secure, Shi2016edge}. 

It is worth noting that, from the point of view of the open security challenges and of the existing solutions, the two above paradigms are substantially identical. In fact, they are both characterized by the adoption of low-range, low-power communication technologies, by the involvement of heterogeneous and often resource-constrained devices (e.g., sensors, actuators, RFID-based systems, etc.), by the typically autonomous nature of such devices and the lack of a centralized control, and by the processing of sensitive information (belonging for example to the private sphere, the daily routines, the industrial systems or the critical infrastructures). 
Due to these similarities, in order to have an acceptable view of existing approaches to CE security, it is appropriate to consider relevant work from the literature in both Edge and Fog contexts.
In \cite{ni2017securing, roman2018mobile, lin2017survey, khan2018iot}, the authors discuss the main security threats of Fog/Edge and general IoT systems. As it will be described in the following section, they include tampering with physical devices and/or transmitted data, forging fake identities and information, jamming communications channels, overloading the nodes with excessive requests in order to exhaust the limited resources of weaker nodes, etc.

Authentication is one of the major security challenges, and has been recently addressed by several scientific studies \cite{Atwady2017authSurvey, Trnka2018AuthSurvey}.
%\cite{stojmenovic2015overview}
In order to prevent illegitimate service access via, for example, \textit{man-in-the-middle} attacks, not only an identity must be assigned to each node, but the nodes must also be able to mutually authenticate with one another, typically without the user intervention and without relying upon a central authentication authority for scalability purposes.
%
Moreover, as anticipated, smart objects typically have energy, memory, computational speed and communications bandwidth constraints which impose the adoption of lightweight operations, not based on traditional cryptographic mechanisms. In this context, embedded and hardware security approaches can be leveraged to build a secure IoT system while taking into account all the existing constraints.

\subsection{Security Risks in the IoT Architecture}
%parla delle attuali soluzioni che fanno uso di crittografia
As described in \cite{lin2017survey,kanuparthi2013hardware} several threats may be addressed to the different layers of a CE-based IoT architecture. Regarding the Edge devices level, as it has the responsibility to interact directly with the environment, its main task is to collect data and actuate commands toward it. The main risks are destruction or alteration of the perception devices and forging of collected data. The main security issues for the lower layer are described below.

\begin{itemize}
	\item \textbf{Node capture attack:} an adversary may be able to physically access the IoT device, in order to replace it or to tamper its hardware. In this case, sensible information are exposed to the attacker, such as authentication keys. By stealing these values, the attacker can copy them to a fake node and get authenticated by the system. This kind of threat is called \textit{node replication attack} and it can be mitigated by using anti-tamper hardware and appropriate scheme for monitoring malicious nodes. It must be remarked that also fog nodes are exposed to this kind of attacks, as differently from the cloud, they are not secured from physical access.
	\item \textbf{False data and code injection attack:} once the node has been captured, the attack may also inject malicious code into the device memory, gaining access to the entire network. This threat can be addressed by means of code authentication. Furthermore, the attacker can be able to replace valid data with fake values and let the device transmit it to the network, thus providing wrong services.
	\item \textbf{Replay attack:} the attacker may use a malicious fake node to send data with legitimate identification information to a node, in order to obtain trust of the system. This kind of issues can be handled by using timestamps and sequence numbers.
	\item \textbf{Eavesdropping and interference:} as most of IoT devices communicates through wireless networks, their communications are exposed to eavesdropping by unauthorized users. Furthermore, the attacker may interfere by sending noise data. 
\end{itemize}

Regarding the middle layer, as it is composed of fog nodes, which main task is to deliver commands to the actuators, filter and aggregate data received from the lower layer, a malicious fog node (\textit{Rogue Fog Node} \cite{yi2015security,roman2018mobile}) may act as a \textit{man-in-the middle}. In this case, once the attacker is able to control a fog node, it can manipulate incoming data from both the devices and the cloud, which cannot detect the intrusion, and exploit the in order to launch further attacks. Moreover, the man-in-the-middle attack can be launched by only relying upon protocols used for the communication.

Furthermore, as fog nodes are hot spots that enable many device to communicate, they are also subject to attacks aimed to impact the availability of the network.
%
%\section{Authentication Protocols for Edge Computing}
%
\section{Hardware-based Authentication Solutions: RFIDs and PUFs}
A technologically mature hardware-based approach to IoT security relies upon the adoption of RFIDs in conjunction with traditional or lightweight cryptographic primitives. 
The work presented in \cite{chien2007rfid} provides a taxonomy of existing RFID-based authentication solutions, ranging from protocols that use classical cryptographic encryption and decryption protocols and have large computational overhead, to those adopting simple functions like random number generators, checksums, etc. or even bit-wise operations such as OR, AND, XOR, rotations and permutations, which have the lowest overhead in terms of storage and computation. 
In \cite{Tewari2017}, an ultra-lightweight RFID-based mutual authentication protocol that uses only bit-wise XOR operations and left-rotations is presented. The authors prove that their protocol ensures data confidentiality, integrity and tag anonymity and resistance
to tracking, and that it is secure against various attacks such as man-in-the-middle, replay, and disclosure attacks.

%However, the adoption of such solutions in modern IoT systems is still an open problem due to the devices' limitations, as outlined in 
However, as discussed in \cite{Kulkarni2014RFIDissues}, RFID tags are inherently subject to several attacks due to their physical nature: 
%being the communication between the RFID tags and the reader radio-based, it can be simply disrupted via jamming attacks; moreover, 
for example, the transponder of an RFID tag, which unambiguously identifies the tagged item, may be simply detached from the object and associated with a different item in an unattended environment; moreover, tags may be cloned with a process that first captures the data from a legitimate tag and then creates an unauthorized copy of the captured sample on a new chip. 
Despite the multiple benefits introduced by the RFID technology, the above mentioned issues suggest that it may introduce serious security and privacy risks if not properly handled. Moreover, it must be noted that RFID-based authentication techniques do not allow for mutual authentication while only enabling a reader to authenticate the objects provided with an RFID tag and not vice-versa.

A promising hardware-based solution that is being recently used for device authentication and that is not subject to the discussed problems relies upon Physically Unclonable Functions \cite{pappu2002physical}. PUFs are represented by integrated circuits characterized by uniqueness and unclonability features conferred by the manufacturing process, such that reproducing or guessing the behavior of a PUF by observing related input-output pairs is computationally hard. 
PUF responses are a good source of randomness, can be hardly guessed, and can be generated on demand when PUFs are powered-up, without the need for a non-volatile memory for their storage, in addition to providing tamper-proof evidence. This makes PUFs particularly suited to be used for the identification and authentication of the devices embedding them, as shown by several scientific results (such as \cite{suh2007physical,frikken2009robust,rostami2014robust,barbareschi2015authenticating}).

