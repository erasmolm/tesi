\chapter{Physically Unclonable Functions (PUFs)}
\label{sec:ch_pufs}
PUFs are hardware primitives usually implemented in integrated circuits that map a set of inputs, namely challenges, into a set of outputs, namely responses. As stated in \cite{barbareschi2017designing}, a PUF behavior depends on unpredictable factors induced during the IC manufacturing, in which the lithographic process introduces random imperfections in dies across a wafer and from wafer to wafer.
PUFs behaves similarly to one-way functions, since mapping between challenges and responses cannot be modeled by means of invertible mathematical functions. For a given device endorsing a PUF, the way a set of challenges is mapped by the PUF into a set of responses is unique for that specific device and can be used for its identification. Given the physical nature of a PUF behavior, any malicious alteration of the its structure would change the mapping mechanism, thus making the PUF tamper-evident.
Quality of a PUF architecture can be estimated by mean of several metrics.

\begin{itemize}
    \item \textit{uniqueness}, which indicates the randomness introduced by the lithographic process among different instances of the same PUF structure;
    \item \textit{reliability}, which quantifies how the PUF tolerates variations of the environmental conditions (voltage, temperature, aging);
    \item \textit{uniformity}, which estimates the distribution of 1s and 0s in the responses.
\end{itemize}

High levels of reliability and uniqueness are obviously desired. On the other hand, uniformity should be as close as possible to 50$\%$, in order to obtain highest randomness.\\
%
PUFs can be classified into \textit{strong} and \textit{weak} PUFs. The latter can support only a limited number of challenges, while strong PUFs have a larger CRP (Challenge-Response) set. Therefore, while it is possible to obtain an exhaustive measurement of the entire CRP set from a weak PUF, for a strong PUF it would result computationally unfeasible. However, strong PUFs are still susceptible to \textit{modeling attacks}, which adopt complex machine learning techniques to obtain a numerical model of the challenge-response relationship, exploiting a reasonably small amount of CRPs. In order to avoid such a kind of attacks, it is necessary to hide the real PUF behavior. This can be achieved realizing what is called a \textit{Controlled PUF} by masking both challenges and responses with a hashing function.\\
%
When a strong PUF is not available, as Bhargava et al. proved in \cite{bhargava2014efficient}, it is still possible to reproduce the behavior of a strong PUF by combining a weak PUF and a symmetric cipher such as AES.\\
\begin{figure}[!htbp]
	\centering
	\includegraphics[width=.5\textwidth]{figures/weak_puf.pdf}
	\caption{weak PUF combined with AES cipher}
	\label{fig:weak_puf}
\end{figure}
%

%
\section{PUF-based Authentication}
\label{sec:puf_auth}
According to \cite{barbareschi2018puf}, CRPs that characterize a PUF represent its own signature and can be used to prove the identity of the device which is endorsing that specific PUF instance. Therefore, devices can be authenticated exploiting their embedded PUF circuit.\\
%
Let us consider an authenticator node, namely \textit{verifier}, and a device embedding a PUF circuit.
In order to authenticated the device to the verifier, they need to share some knowledge strictly related to the device, for example one or more CRPs. In a basic authentication protocol, the authenticator node submits a \textit{known} challenge to the device and compares the response given by this one with the one expected.\\
%
Authors in \cite{suh2007physical} proposed an authentication mechanism based on strong PUFs composed of two different phases:
\begin{figure}[!htbp]
    \centering
	\includegraphics[width=.8\textwidth]{figures/puf_auth.pdf}
	\caption{PUF-based authentication mechanism}
	\label{fig:PUF_auth}
\end{figure}

\begin{enumerate}
    \item \textbf{Enrollment:} in a trusted environment, a Trusted Third Party (TTP), for instance the verifier, can physically accesses to the device embedding the PUF and applies a significant amount of challenges in order to collect the respective responses. The CRPs are stored in a secure database for subsequent authentication requests.
    \item \textbf{Verification:} each time the TTP needs to authenticate the device remotely, it applies a new challenge, selected from the previously collected challenges, to the device and obtains a PUF response from it. In order to send challenges and responses in clear-text, they must never have been used before. Once the obtained the response from the device, it is compared with the one stored into the secure database. If the comparison succeeds, the TTP can prove device authenticity, since the CRP is known only by the verifier and the authentic device.
\end{enumerate}

\section{Mutual Authentication}
The previously exposed authentication mechanism can be adapted to achieve mutual authentication in a one-to-many scenario, where a large set of \textit{stateless} devices with very few computational resources exchange messages with a sink node with high computational capabilities. In this scenario, PUFs provide an efficient solution to the mutual authentication problem, without resorting to cryptography and therefore not consequently introducing overhead due to it. 

Let us consider the following notation:
\begin{itemize}
    \item let $\theta_D(\cdot)$ be the PUF embedded within the device D
    \item let $C$ be the set of challenges for $\theta_D(\cdot)$
    \item let $\overline{C} \subset C$ be the set of challenges applied to $\theta_D(\cdot)$ by the verifier throughout the enrollment phase
\end{itemize}
As a consequence, given a challenge $c \subset \overline{C}$ the verifier authenticates the device D if the response, $r$ computed as $\theta_D(c)$, equals to the response stored by the verifier for $c$ during the enrollment phase. This result proves to the verifier that the device D embeds the $\theta_D(\cdot)$ PUF, which is unique for each device.

On the other way around, in order to prove the verifier identity to the device D, it is necessary for the first to prove that it previously enrolled the $\theta_D(\cdot)$ PUF. Hence, the verifier must pick from the database the right response for a given challenge $c$ such as $c \subset \overline{C}$.

However, keeping in mind that the device D is stateless, it is not aware of the CRPs enrolled by the verifier. Furthermore, it is not feasible to store the CRPs onto the device.

This issue can be overcome providing devices with strictly necessary knowledge to authenticate the verifier, without requiring great computation and storage capabilities.

Bearing in mind that $R \subseteq C$, sets $R$ and $C$ partially overlap. In the graph-based representation of the $\theta_D(\cdot)$ PUF, this paths identify \textit{chains} of links that can be obtained by self-iterating the $\theta_D(\cdot)$ PUF multiple times starting from a challenge $c_0$ (referred to as the chain \textit{root}).

\begin{figure}[!h]
	\centering
	\includegraphics[width=.5\textwidth]{puf_example_letters.pdf}
	\caption{An example of PUF embedded by a device D}
	\label{fig:puf_letters}
\end{figure}

Figure~\ref{fig:puf_letters} illustrates an example of PUF where, for the sake of simplicity, the characters belonging to the Latin alphabet where used in place of binary values. 
All the possible simple paths that can be identified in the graph representation corresponding to the PUF definition.

PUF chains can significantly facilitate management of the CRPs for both the entities. At the time of the enrollment phase, the verifier, despite applying random challenges to the $\theta_D(\cdot)$ PUF, it can select a random challenge $c_o$ and generate a chain by submitting the previous obtained response as a new challenge. In this way, synchronizing both the entities on the same node of a chain, the device can authenticate the counterpart by submitting a challenge (a node) taken from that chain and make sure that the verifier knows the next node.
