\chapter{Efficiency Analysis and Experimental Results}
\label{sec:ch_perf}
%\section{A Use Case based on STM32L4 over 3G connectivity}
%\section{A Use Case based on STM32F7 over Ethernet Connectivity}
%\section{Latency Evaluation Metrics}
\section{Implementation}
\label{sec:result}

As stated in Section \ref{sec:ph}, the proposed mutual authentication scheme assumes the adoption of strong PUFs, able to provide a reliable response
each time they are presented with a binary stimulus. When strong PUFs are not available, a hardware component that combines a weak PUF and a symmetric cipher may be used, as discussed in \cite{bhargava2014efficient}. This solution, relying upon an SRAM PUF, was adopted to implement the extended PHEMAP scheme and carry out experiments.

In particular, the aforementioned PUF architecture was implemented on two different types of micro-controllers, representing two terminal devices with different capabilities.
On the one hand, the STMicroelectronics \texttt{STM32F7} board has been adopted as terminal node. This board provides:

\begin{itemize}
	\item a 32-bit ARM Cortex M7 CPU 216 MHz
	\item 340 Kbytes of RAM
	\item 1 Mbytes of Flash memory
	\item IEEE 802.3 Ethernet connector
\end{itemize}

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.5\textwidth]{figures/f7.jpeg}
	\caption{STM32F7 Discovery}
	\label{fig:stm_f7}
\end{figure}


On the other hand, the STMicroelectronics \texttt{STM32L4} board has been adopted as terminal node with 2G/3G connectivity. This board provides:

\begin{itemize}
	\item a 32-bit ARM Cortex M4 MCU 80 MHz
	\item 320 Kbytes of RAM
	\item 1 Mbytes of Flash memory
	\item Quectel UG96 worldwide cellular modem penta-band 2G/3G module, 7.2 Mbps downlink, 5.76 Mbps uplink
\end{itemize}

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.7\textwidth]{figures/l4.png}
	\caption{STM32L4 Discovery}
	\label{fig:stm_l4}
\end{figure}

Both the STM32 boards relies upon LwIP \ref{sec:lwip} implementation running on FreeRTOS, the network task is based upon the BSD Socket API, which is built upon the Netconn API.
Finally, the Cubieboard \textit{Cubietruck} board has been adopted both as a device and as a gateway. This board resources are those typically found in a modern cell phone, it comes with an Ubuntu 18.04 OS installed on a micro SD and it is equipped with:

\begin{itemize}
	\item a 64-bit octacore ARM Cortex (A15x4 up to 2.0GHz, A7x4 up to 1.3GHz) CPU
	\item 2GB of RAM
	\item 8GB of eMMC storage memory
	\item 10M/100M/1GB NIC Ethernet connection
\end{itemize}

\begin{figure}[!h]
	\centering
	\includegraphics[width=0.7\textwidth]{figures/ct.png}
	\caption{Cubieboard Cubietruck}
	\label{fig:ct}
\end{figure}


The authentication service was implemented through a software application written in C++ language, configured as a server application able to serve multiple requests coming from devices. In the first set of experiments aimed to assessing BC PHEMAP performances, the Authentication Service application was deployed on an Azure \texttt{D2s Standard v3} virtual machine (provided with 2 virtual CPUs and 8 GB of memory), running an Ubuntu 18.04 64 bit OS distribution. 
As for the chains database, the file system offered by the operating system has been used, without installing any database server in order to reduce the access overhead. The second set of experiments, aimed to assessing SALTED PHEMAP performances, the Authentication Service was deployed on an Amazon Web Service (AWS) \textit{EC2 t2.micro} virtual machine. It is provided with a single virtual CPU, 1 GB of RAM.

\section{Experimental evaluation}
The efficiency and the general performance of PHEMAP were extensively discussed in \cite{barbareschi2018puf} and remarked in section \ref{sec:phemap_performance}, where it was demonstrated that the computation and
hardware overhead introduced by the protocol makes it feasible for commercial mid-range devices.
This section shows how the extended scheme proposed in this work enables to obtain better performance in the considered CE-based IoT scenario, with special regard to the communication between terminal devices.

\subsection{Assessing PHEMAP performance}
In order to enable two terminal devices to communicate with the basic PHEMAP, the AS should act as an intermediary and relay messages between A and B by assessing the identities of the two devices. Assuming that the device A wants to send a message to device B: the main interactions are sketched in Figure \ref{fig:ABphemap}, which simply reports the authentication information (i.e., the chain links) exchanged by the parties to accomplish mutual authentication between the pairs (A,AS) and (AS,B).

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{ABphemap.pdf}
	\caption{Communication between two devices with PHEMAP}
	\label{fig:ABphemap}
\end{figure}

Based on these interactions, the time $t^{PHEMAP}_{\left(A,B\right)}$ needed to send a message from A to B and complete a mutual authentication via AS would include (i) the time needed to initialize both A and B ($t^{init}_{\left(AS,A\right)}$ and $t^{init}_{\left(AS,B\right)}$, respectively) plus (ii) the time needed to convey the message from A to AS and then from AS to B and perform the related mutual authentication operations. 
The latter latency simply corresponds to the latency of two subsequent PHEMAP verification operations. 
Without loss of generality, we can assume that the communication latency between A and AS and the communication latency between B and AS are the same, hence the verification latency can be denoted as  $t^{PHEMAP}_{\left(AS,D\right)}$ and referred to a generic device D. Moreover, we can assume that initialization is performed simultaneously on the two devices, and denote related latency with $t^{init}_{\left(AS,D\right)}$. 
If follows that $t^{PHEMAP}_{\left(A,B\right)}$ can be computed as:

\begin{equation}
t^{PHEMAP}_{\left(A,B\right)} = t^{init}_{\left(AS,D\right)} + 2* t^{PHEMAP}_{\left(AS,D\right)}
\label{eq:phemaptemp}
\end{equation}

Clearly, if A wants to send $m$ messages to B (i.e., $m$ mutual authentication operations must be completed), the overall latency will be equal to:

\begin{equation}
t^{PHEMAP}_{\left(A,B\right)} (m) = t^{init}_{\left(AS,D\right)} + 2* m *t^{PHEMAP}_{\left(AS,D\right)}
\label{eq:phemaptemp_m}
\end{equation}

In the first set of experiments, the basic PHEMAP protocol has been assessed on the considered experimental setup in order to evaluate, in particular, the average time needed to accomplish a mutual authentication between the verifier, represented by the AS application running on the cloud, and a basic terminal node implemented by the STM32 device. 

Figure \ref{fig:phemap_auth_time} reports the latency measured in 1000 distinct consecutive mutual authentication operations (i.e., PHEMAP verification steps), whose average results to be about 98.46 milliseconds.

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{test1.pdf}
	\caption{PHEMAP verification latency between a device and the AS}
	\label{fig:phemap_auth_time}
\end{figure}

It must be recalled that both the basic PHEMAP protocol and the introduced extensions require the authentication service and the device to be synchronized on the same chain link. Figure \ref{fig:phemapinit} reports the PHEMAP initialization latency measured by launching 600 subsequent initialization steps with $S=4$ (sentinel period). The average initialization time results to be around 99.83 milliseconds. 

\begin{figure}[!h]
	\centering
	\includegraphics[width=\textwidth]{test1b.pdf}
	\caption{PHEMAP initialization latency}
	\label{fig:phemapinit}
\end{figure}


Roughly, in the adopted experimental setup, the above latency introduced in \ref{eq:phemaptemp} is about 296,75 milliseconds in average, with 196,92 milliseconds spent only in the verification step.

Table summarizes the terms calculated above.

%
\begin{table}[htbp]
	\footnotesize
	\centering
	\begin{tabular}{|l|l|}
		\hline
		\textbf{Symbol} & \textbf{Measured Value}	\\ \hline
		$\bar{t}_{A,B}^{PHEMAP}$	& $296.75 ms$	\\ \hline
		$\bar{t}_{AS,D}^{PHEMAP}$	& $98.46 ms$	\\ \hline
		$\bar{t}_{AS,D}^{init}$		& $99.83 ms$	\\ \hline
	\end{tabular}
	\caption{Basic PHEMAP performances}
	\label{tab:phemap_perf}
\end{table}
\normalsize
%

\subsection{Assessing pairwise BC PHEMAP performance}

In order to evaluate the performance of the introduced mutual authentication scheme and compare it with the basic PHEMAP protocol, let us consider BC PHEMAP first and let us assume, again, that A wants to send a message to B. 
Recalling that, apart from the PHEMAP initialization process, that must be completed to enable subsequent operations, BC PHEMAP requires also a setup procedure in order to provide device A with a carnet.\\
Let us denote with $t^{BCsetup}_{\left(A,AS\right)}(T)$ the time needed to complete a setup procedure involving a carnet of $T$ tickets. 
After setup, the two devices can communicate directly by means of a simple exchange of links (tickets for device A) that resembles the basic PHEMAP verification scheme. Let us denote with $t^{mAuth}_{\left(A,B\right)}$ the associated latency.

The time needed to send a message from A to B and to complete the related mutual authentication task is the following:

\begin{equation}
t^{BC PHEMAP}_{\left(A,B\right)} (T)= t^{init}_{\left(AS,D\right)} + t^{BCsetup}_{\left(A,AS\right)}(T)+ t^{mAuth}_{\left(A,B\right)} 
\label{eq:bc_temp}
\end{equation}\\


Note that each message exchange, i.e., each mutual authentication operation, requires device A to consume two subsequent tickets of the carnet.  Once all the links of the carnet have been consumed, another setup procedure must be launched, to provide device A with a brand new carnet.
Therefore, in case A wants to send $m \leq T/2$ messages to B, the overall latency will simply be equal to:

\begin{equation}
t^{BC PHEMAP}_{\left(A,B\right)}(T,m) = t^{init}_{\left(AS,D\right)} + t^{BCsetup}_{\left(A,AS\right)}(T) + m * t^{mAuth}_{\left(A,B\right)} 
\label{eq:bcm}
\end{equation}
In general, when more than $T/2$ messages must be exchanged, it is necessary to repeat the setup phase in order to acquire a new carnet. The above equation then becomes:

\begin{equation}
t^{BC PHEMAP}_{\left(A,B\right)}(T,m) = t^{init}_{\left(AS,D\right)} + \left\lceil \frac{m}{T/2} \right\rceil * t^{BCsetup}_{\left(A,AS\right)} (T) + m * t^{mAuth}_{\left(A,B\right)} 
\label{eq:bc}
\end{equation}

In order to evaluate the performance of BC PHEMAP, the measured average time $t^{mAuth}_{\left(A,B\right)}$, spent for completing a generic mutual authentication, is necessary. In particular, 1000 consecutive mutual authentication operations between a device A implemented by the STM32 boards as devices. 
Figure \ref{fig:bcauthn} shows the obtained results, which can be summarized in an average latency of about 35.58 milliseconds. This measure  represents the 36\% of the basic PHEMAP verification latency resulting from the previously described experiment. 

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{test2.pdf}
	\caption{BC PHEMAP mutual authentication latency}
	\label{fig:bcauthn}
\end{figure}

While the above result demonstrates that BC PHEMAP outperforms PHEMAP with regard to the verification latency, it is worth noting that the partial delegation of authentication capabilities introduced with the proposed scheme is subject to a trade-off, represented by the need for managing and transferring a carnet of links and by the availability of a limited number of tickets to spend for communication. 

In order to analyze such trade-off, figure \ref{fig:bc-vary} reports the time spent to send a fixed number of messages from A to B when varying the carnet length (without considering the fixed overhead due to initialization).

\begin{figure}[!h]
	\centering
	\includegraphics[width=.9\textwidth]{test3.pdf}
	\caption{BC PHEMAP performance with respect to the carnet length}
	\label{fig:bc-vary}
\end{figure}

In particular, fixed the number of messages to exchange to $m=500$ and let the carnet length $T$ assume the following values: $\left[20,50,100,200,250,500\right]$. The results of this experiment are depicted in Figure \ref{fig:bc-vary}, which shows the latency trend when varying the carnet length:  when $T=500$, two setup steps must be launched, each one requiring about 500 milliseconds to complete as it involves the transfer of a carnet of 8KBytes from the cloud-based AS. As shown in the picture, a very similar performance is obtained for lower values of $T$: the breakpoint is represented by the value $T=100$, which implies that a carnet only allows for the exchange of 50 messages, therefore the BC setup procedure must be launched 10 times. When reducing further the carnet length, the performance significantly decreases as more interactions with the cloud service are needed.
It is worth pointing out that the carnet length is a fundamental parameter that can be tuned based on the existing constraints both in terms of latency and in terms of security: if the communications among terminal nodes are critical and the risk of having them compromised is high, the system manager can prefer a short carnet and accept to pay the resulting increment in latency. 


\begin{figure}[!h]
	\centering
	\includegraphics[width=1.1\textwidth]{test4.pdf}
	\caption{BC PHEMAP performance when varying the number of messages and the carnet length}
	\label{fig:bc-vary2}
\end{figure}

Figure \ref{fig:bc-vary2} provides another point of view on the behavior of BC PHEMAP when considering different carnet lengths with respect to different numbers of exchanged messages. In particular, let the number of messages vary, the curves related to the latency of BC PHEMAP when $T=20, 100$ and $500$ respectively were plotted. 
As shown, while PHEMAP latency always increases as $m$ grows, the behavior of BC PHEMAP depends on the value of $T$ and on the number of setup operations that must be carried out. In particular, when the number of exchanged messages is relatively small, a counter-intuitive behavior compared to the one observed in Figure \ref{fig:bc-vary} can be found. As shown, when $m<100$, the latency increases when $T$ grows rather than decreasing. This is due to the latency of the setup phase, which represents the prevalent contribution as it involves the communication with the cloud service.

For all the three considered values of $T$, only one setup is needed and the different latency measures basically depend on the size of the transferred carnet. 
For $m=20$, when $T=20$ is selected, two setup steps are required, which makes the performance of the protocol worse than in the case of $T=100$, as expected.
For higher values of $m$, the difference in the resulting latency is mainly due to the overhead of transmitting carnets of different sizes during the setup. When the number of messages to transmit increases, the curves related to $T=50$ and $T=100$ have a similar behavior.

\subsection{Assessing Proxy-based BC PHEMAP performance}

This section provides the performance of the \textit{Proxy-based BC-PHEMAP} protocol variant. In particular, devices A and B is implemented by the STM32 boards, the gateway is implemented by the Cubietruck.  
The mutual authentication latency associated with this protocol when exchanging $m$ messages from device A to device B through a gateway G includes three main contributions:\\

\renewcommand{\labelenumi}{\roman{enumi}}
\begin{enumerate}
	\item latency of the PHEMAP initialization $t^{init}_{\left(AS,D\right)}$;
	\item the latency of the Proxy BC PHEMAP setup, needed to provide G with a carnet, denoted with $t^{PBCsetup}_{\left(G,AS\right)}(T)$;
	\item the latency of the communication between A and B via G.
\end{enumerate}

Note that the third contribution essentially includes the normal interactions that would happen between the two devices during the basic BC PHEMAP verification phase (considering that the slight different operations performed there by device A are negligible with regard to latency), which is essentially equal to $t^{mAuth}_{\left(A,B\right)}$, introduced before. Moreover, it includes the time needed to forward messages to and from G. Assuming that this time is equal for the pairs (A,G) and (B,G) and denoted with $t^{comm}_{\left(D,G\right)}$, the overall latency is given by:

\begin{equation}
t^{PBC PHEMAP}_{\left(A,B\right)}(T,m) = t^{init}_{\left(AS,D\right)} + \left\lceil \frac{m}{T/2} \right\rceil * t^{PBCsetup}_{\left(G,AS\right)} (T) + m * t^{mAuth}_{\left(A,B\right)} + 2 * m * t^{comm}_{\left(D,G\right)}
\label{eq:bc}
\end{equation}\\

It is easy to conclude that the considerations made for BC PHEMAP can be easily applied to the proxy variant with minor changes, since the overhead of communication among edge devices can be assumed to be quite low.

\subsection{Assessing Salted PHEMAP performance}
In order to evaluate the performance of Salted PHEMAP and compare it with the basic PHEMAP protocol, let us assume that A wants to send $m=1000$ messages to AS through a gateway G, for a total of $11893 KB$ of data. 

In case of the basic PHEMAP protocol, the device A should send every authenticated message directly to AS.
Considering the distance between the Cloud Server and the device, this message exchange is affected by a considerable latency.

On the contrary, by sending authenticated messages to the gateway at first and then transferring the entire block of data from G to AS could reduce the overall delay.

As said in the previous section, the following experiments were conducted using the STM32F7 as device, the Cubietruck as gateway and the Amazon EC2 virtual machine. It must be noted that, compared to the Azure virtual machine, the EC2 has less computational power.

As for BC PHEMAP, apart from the PHEMAP initialization process, that must be completed to enable subsequent operations, Salted PHEMAP requires also a setup procedure in order to provide the gateway with a carnet.

Let us denote with $t^{Ssetup}_{\left(A,AS\right)}(T)$ the time needed to complete a setup procedure involving a carnet of $T$ tickets.

After setup, the device A can communicate with the gateway G directly by means of a simple exchange of links (tickets for device A) that resembles the basic PHEMAP verification scheme. Let us denote with $t^{mAuth}_{\left(A,G\right)}$ the associated latency.

With reference to the \ref{eq:phemaptemp} expression, when using basic PHEMAP, the time needed to send a single message from device A to AS equals to:

\begin{equation}
t^{PHEMAP}_{\left(A,AS\right)} = t^{init}_{\left(AS,A\right)} + t^{PHEMAP}_{\left(A,AS\right)} 
\label{eq:phemaptemp_a_as}
\end{equation}

Moreover, to send $m$ messages from A to AS, the latency equals to:

\begin{equation}
t^{PHEMAP}_{\left(A,AS\right)} (m) = t^{init}_{\left(AS,A\right)} + m *t^{PHEMAP}_{\left(A,AS\right)}
\label{eq:phemaptemp_m_as_a}
\end{equation}

On the other hand, when using Salted PHEMAP, the time needed to send a single message from A to G, given a carnet of length $T$, and to complete the related mutual authentication task is the following:

\begin{equation}
t^{SALTED}_{\left(A,G\right)} (T)= t^{init}_{\left(AS,A\right)} + t^{Ssetup}_{\left(G,AS\right)}(T)+ t^{mAuth}_{\left(A,G\right)} 
%\label{eq:salted_a_g}
\end{equation}

As for BC PHEMAP, each message exchange, i.e., each mutual authentication operation, requires G to consume two subsequent tickets of the carnet.  Once all the links of the carnet have been consumed, another setup procedure must be launched, to provide G with a brand new carnet. Moreover, note that, when using Salted PHEMAP, the transfer involves also sending the block of $m$ messages from G to AS. The corresponding time is denoted with $m*t^{trans}_{G,AS}$

Therefore, in case A wants to send $m \leq T/2$ messages to AS, the overall latency will be equal to:

\begin{equation}
t^{SALTED}_{\left(A,AS\right)}(T,m) = t^{init}_{\left(AS,A\right)} + t^{Ssetup}_{\left(G,AS\right)}(T) + m * t^{mAuth}_{\left(A,G\right)} + m*t^{trans}_{G,AS}
%\label{eq:salted_a_g_m}
\end{equation}

In general, when more than $T/2$ messages must be exchanged, it is necessary to repeat the setup phase in order to acquire a new carnet. The above equation then becomes:

\begin{equation}
t^{SALTED}_{\left(A,AS\right)}(T,m) = t^{init}_{\left(AS,A\right)} + \left\lceil \frac{m}{T/2} \right\rceil * t^{Ssetup}_{\left(G,AS\right)} (T) + m * t^{mAuth}_{\left(A,G\right)} + m*t^{trans}_{G,AS} 
%\label{eq:salted_a_g_m_general}
\end{equation}

In order to evaluate the performance of Salted PHEMAP, the aforementioned example was implemented using both basic and salted PHEMAP. Figure \ref{fig:basic_ver_aws} shows the obtained results for $t^{PHEMAP}_{\left(A,AS\right)} (m)$ with $m=1000$ using the AWS EC2 virtual machine, which can be summarized in an average latency of about $111.75 ms$. Figure \ref{fig:basic_init_aws} shows latency for $1000$ PHEMAP initializations, with an average time of $119.81 ms$. Figure \ref{fig:salted_ver_aws} shows the latency corresponding to $1000$ messages sent from device A to gateway G with an average time of about $35 ms$, as for the previous setup. Despite the previous two measures this last one remain unchanged because both device A and the gateway are connected to the same local network, thus they are not affected by the different performance of the authentication server.

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{figures/ver_basic.pdf}
	\caption{PHEMAP mutual authentication latency using AWS EC2 as Server}
	\label{fig:basic_ver_aws}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{figures/init_phemap.pdf}
	\caption{PHEMAP initialization latency using AWS EC2 as Server}
	\label{fig:basic_init_aws}
\end{figure}

\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{figures/ver_salted.pdf}
	\caption{Salted PHEMAP mutual authentication latency}
	\label{fig:salted_ver_aws}
\end{figure}

The above result demonstrates that Salted PHEMAP outperforms PHEMAP with regard to the verification latency. However, it is worth noting that, as for BC PHEMAP, the partial delegation of authentication capabilities introduced with the proposed scheme is subject to a trade-off, represented by the need for managing and transferring a carnet of links and by the availability of a limited number of tickets to spend for communication. 

Such trade-off is analyzed in figure \ref{fig:salted_vary} reports the time spent to send a fixed number of messages from A to B when varying the carnet length (without considering the fixed overhead due to initialization).

\begin{figure}[!h]
	\centering
	\includegraphics[width=.9\textwidth]{t_curve.pdf}
	\caption{Salted PHEMAP performance with respect to the carnet length}
	\label{fig:salted_vary}
\end{figure}

In particular, fixed the number of messages to exchange to $m=500$ and let the carnet length $T$ assume the following values: $\left[20,50,100,200,250,500\right]$. As expected, the behavior is the same as for BabelChain. Figure \ref{fig:bc-vary} shows the latency trend when varying the carnet length: when $T=500$, two setup steps must be launched, each one requiring about 330 milliseconds to complete as it involves the transfer of a carnet of 8KBytes from the cloud-based AS. As shown in the picture, a very similar performance is obtained for lower values of $T$: the breakpoint is represented by the value $T=100$, which implies that a carnet only allows for the exchange of 50 messages, therefore the Salt setup procedure must be launched 10 times. When reducing further the carnet length, the performance significantly decreases as more interactions with the cloud service are needed.
Again, the same considerations as for BC PHEMAP can be made for the salted carnet length: it is a fundamental parameter that can be tuned based on the existing constraints both in terms of latency and in terms of security. If the communications among terminal nodes are critical and the risk of having them compromised is high, the system manager can prefer a short carnet and accept to pay the resulting increment in latency. 

\begin{figure}[!h]
	\centering
	\includegraphics[width=1.1\textwidth]{compare_salted.pdf}
	\caption{BC PHEMAP performance when varying the number of messages and the carnet length}
	\label{fig:salted_vary2}
\end{figure}

As for BC PHEMAP, figure \ref{fig:salted_vary2} represents the behavior of salted PHEMAP when considering different carnet lengths with respect to different numbers of exchanged messages. In particular, let the number of messages vary, the curves related to the latency of BC PHEMAP when $T=20, 100$ and $500$ respectively were plotted. 
As shown, while PHEMAP latency always increases as $m$ grows, the behavior of salted PHEMAP depends on the value of $T$ and on the number of setup operations that must be carried out. In particular, when the number of exchanged messages is relatively small, a counter-intuitive behavior compared to the one observed in Figure \ref{fig:salted_vary} can be found. As shown, when $m<100$, the latency increases when $T$ grows rather than decreasing. This is due to the latency of the setup phase, which represents the prevalent contribution as it involves the communication with the cloud service.

For all the three considered values of $T$, only one setup is needed and the different latency measures basically depend on the size of the transferred carnet. 
For $m=20$, when $T=20$ is selected, two setup steps are required, which makes the performance of the protocol worse than in the case of $T=100$, as expected.
For higher values of $m$, the difference in the resulting latency is mainly due to the overhead of transmitting carnets of different sizes during the setup. When the number of messages to transmit increases, the curves related to $T=50$ and $T=100$ have a similar behavior.



%
