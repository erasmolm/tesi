\contentsline {chapter}{Contents}{i}{section*.1}
\contentsline {chapter}{List of Figures}{iii}{section*.3}
\contentsline {chapter}{Abstract}{1}{chapter*.6}
\contentsline {chapter}{\numberline {1}Introduction}{3}{chapter.1}
\contentsline {chapter}{\numberline {2}Background on CE and Authentication Protocols}{9}{chapter.2}
\contentsline {section}{\numberline {2.1}Cloud-Edge IoT Systems Security issues}{9}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Security Risks in the IoT Architecture}{10}{subsection.2.1.1}
\contentsline {section}{\numberline {2.2}Hardware-based Authentication Solutions: RFIDs and PUFs}{12}{section.2.2}
\contentsline {chapter}{\numberline {3}Physically Unclonable Functions (PUFs)}{14}{chapter.3}
\contentsline {section}{\numberline {3.1}PUF-based Authentication}{16}{section.3.1}
\contentsline {section}{\numberline {3.2}Mutual Authentication}{17}{section.3.2}
\contentsline {section}{\numberline {3.3}PHEMAP protocol}{19}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Notation and technical concepts: chains and sentinels}{20}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Assumptions}{22}{subsection.3.3.2}
\contentsline {subsection}{\numberline {3.3.3}Enrollment}{24}{subsection.3.3.3}
\contentsline {subsection}{\numberline {3.3.4}Initialization}{25}{subsection.3.3.4}
\contentsline {subsection}{\numberline {3.3.5}Verification}{27}{subsection.3.3.5}
\contentsline {subsubsection}{Example}{29}{section*.15}
\contentsline {subsection}{\numberline {3.3.6}De-synchronization and chain exhaustion}{30}{subsection.3.3.6}
\contentsline {subsubsection}{De-synchronization during initialization}{31}{section*.17}
\contentsline {subsubsection}{De-synchronization during verification}{32}{section*.18}
\contentsline {subsection}{\numberline {3.3.7}PHEMAP Analysis}{34}{subsection.3.3.7}
\contentsline {subsubsection}{Security Considerations}{34}{section*.19}
\contentsline {subsubsection}{Performance Analysis}{36}{section*.20}
\contentsline {section}{\numberline {3.4}Extended PHEMAP}{39}{section.3.4}
\contentsline {subsection}{\numberline {3.4.1}Salted PHEMAP}{41}{subsection.3.4.1}
\contentsline {subsubsection}{Security considerations}{46}{section*.26}
\contentsline {subsection}{\numberline {3.4.2}Babel-Chain PHEMAP}{47}{subsection.3.4.2}
\contentsline {subsection}{\numberline {3.4.3}Security considerations}{51}{subsection.3.4.3}
\contentsline {chapter}{\numberline {4}Efficiency Analysis and Experimental Results}{53}{chapter.4}
\contentsline {section}{\numberline {4.1}Implementation}{53}{section.4.1}
\contentsline {section}{\numberline {4.2}Experimental evaluation}{56}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Assessing PHEMAP performance}{56}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Assessing pairwise BC PHEMAP performance}{59}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Assessing Proxy-based BC PHEMAP performance}{64}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Assessing Salted PHEMAP performance}{65}{subsection.4.2.4}
\contentsline {chapter}{\numberline {5}Application Domain and Deployment Scheme}{72}{chapter.5}
\contentsline {section}{\numberline {5.1}Continuous Integration}{74}{section.5.1}
\contentsline {section}{\numberline {5.2}Chef Automation}{77}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Chef Workstation Configuration}{79}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Chef Client Configuration}{83}{subsection.5.2.2}
\contentsline {chapter}{\numberline {6}Conclusions}{84}{chapter.6}
\contentsline {section}{\numberline {6.1}Future Direction}{86}{section.6.1}
\contentsline {chapter}{\numberline {7}Appendix A: Chef Automation}{87}{chapter.7}
\contentsline {chapter}{\numberline {8}Appendix B: STM32L4}{90}{chapter.8}
\contentsline {section}{\numberline {8.1}LwIP TCP/IP stack}{92}{section.8.1}
\contentsline {section}{\numberline {8.2}Raw API operational mode}{93}{section.8.2}
\contentsline {section}{\numberline {8.3}Netconn and BSD Socket API operational mode}{94}{section.8.3}
\contentsline {chapter}{Acknowledgement}{96}{chapter*.56}
