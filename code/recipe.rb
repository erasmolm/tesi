
remote_file "/home/erasmo/pufLessPkg.tar" do
	source "https://gitlab.com/erasmolm/phemap/-/jobs/artifacts/salted/raw/cross/pufLessPkg.tar?job=build"
	notifies :run, 'bash[install_script]', :immediately
end

bash "install_script" do
	user "erasmo"
	cwd "/home/erasmo"
	code <<-EOH
	rm -Rf chef_phemap
	mkdir chef_phemap
	tar -xf pufLessPkg.tar -C /home/erasmo/chef_phemap
	rm pufLessPkg.tar
	EOH
	action :nothing
end

template '/home/erasmo/run_phemap.sh' do
	source 'run_phemap.sh.erb'
	owner 'erasmo'
	group 'erasmo'
	mode '0755'
end


